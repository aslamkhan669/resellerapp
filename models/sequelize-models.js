////////////////////////////////////////////////////////////////////
//
// GENERATED CLASS
//
// DO NOT EDIT
//
// See sequelize-auto-ts for edits
//
////////////////////////////////////////////////////////////////////
var Sequelize = require('sequelize');
exports.initialized = false;
function initialize(database, username, password, options) {
   
    if (exports.initialized) {
        return;
    }
    exports.SEQUELIZE = new Sequelize(database, username, password, options);
      exports.customers = exports.SEQUELIZE.define('customers', {
        'id': { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        'name': Sequelize.STRING,
        'email': Sequelize.INTEGER,
        'phone': Sequelize.INTEGER,
        'address': Sequelize.STRING,
        'vendor_id' : Sequelize.STRING,
        'verified': Sequelize.BOOLEAN
       
    }, {
        timestamps: false,
        classMethods: {
            getbanners: function (banner) {
                
            },
            freezeTableName:true
        }
    });
    exports.banner = exports.SEQUELIZE.define('banner', {
        'id': { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        'image': Sequelize.STRING,
        'active': Sequelize.INTEGER,
       
       
    }, {
        timestamps: false,
        classMethods: {
            getbanners: function (banner) {
                
            },
            freezeTableName:true
        }
    });
    exports.category = exports.SEQUELIZE.define('category', {
        'id': { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        'category_name': Sequelize.STRING,
        'category_image': Sequelize.INTEGER,
        'parent_id': Sequelize.INTEGER,
        'description': Sequelize.STRING,
        'weight':Sequelize.INTEGER       
       
    }, {
        timestamps: false,
        classMethods: {
            getbanners: function (banner) {
                
            },
            freezeTableName:true
        }
    });
    exports.products = exports.SEQUELIZE.define('products', {
        'id': { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        'product_name': Sequelize.STRING,
        'product_image': Sequelize.INTEGER,
        'sku': Sequelize.STRING,
        'stock_quantity':Sequelize.INTEGER,
        'product_price': Sequelize.DECIMAL,
        'product_category': Sequelize.INTEGER,
        'product_description': Sequelize.STRING,
        'vendor_id':Sequelize.STRING,
        'created_at': Sequelize.STRING,
        'updated_at': Sequelize.STRING        
       
    }, {
        timestamps: false,
        classMethods: {
            getbanners: function (banner) {
                
            },
            freezeTableName:true
        }
    });
    exports.orders = exports.SEQUELIZE.define('orders', {
        'id': { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        'customer': Sequelize.STRING,
        'phone': Sequelize.INTEGER,
        'email': Sequelize.INTEGER,
        'status': Sequelize.DECIMAL,
        'address': Sequelize.INTEGER,
        'created_at': Sequelize.STRING        
       
    }, {
        timestamps: false,
        classMethods: {
            getbanners: function (banner) {
                
            },
            freezeTableName:true
        }
    });
     exports.orderitems = exports.SEQUELIZE.define('orderitems', {
        'id': { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        'order_id': Sequelize.STRING,
        'product': Sequelize.INTEGER,
        'quantity': Sequelize.INTEGER,
        'price': Sequelize.DECIMAL,
        'product_image': Sequelize.INTEGER,
        'sku': Sequelize.STRING 
       
    }, {
        timestamps: false,
        classMethods: {
            getbanners: function (banner) {
                
            },
            freezeTableName:true
        }
    });
    
    exports.vendors = exports.SEQUELIZE.define('vendors',{
        'id':{type:Sequelize.INTEGER, primaryKey:true , autoIncrement: true},
        // 'product_id':Sequelize.STRING,
        'name':Sequelize.STRING,
        'email':Sequelize.STRING,
        'phone':Sequelize.STRING,
        'address':Sequelize.STRING,
        'Verified' : Sequelize.BOOLEAN,
    }, {
        timestamps:false,
        classMethods : {
              getbanners: function(vendors){

              },
               freezeTableName:true
      }

    });

    exports.prices = exports.SEQUELIZE.define('prices',{
        'id' : {type : Sequelize.INTEGER,primaryKey : true , autoIncrement:true},
        'product_id' : Sequelize.INTEGER,
        'product_price' : Sequelize.INTEGER,
        'vendor_id' : Sequelize.INTEGER,
        'stock_quantity' : Sequelize.INTEGER
    },
    {
        timestamps:false,
        classMethods: {
            getbanners: function (prices) {

            },
            freezeTableName: true
    
    }
});

exports.vendor_orders = exports.SEQUELIZE.define('vendor_orders',{
    'id' : {type : Sequelize.INTEGER,primaryKey : true, autoIncrement : true},
    'vendor_id' : Sequelize.STRING,
    'vendor_name':Sequelize.STRING,
    'phone':Sequelize.STRING,
    'email':Sequelize.STRING,
    'status':Sequelize.STRING,
},
    {
        timestamps: false,
        classMethods: {
            getbanners: function () {

            },
            freezeTableName: true
        

        }
    
});

    exports.vendor_order_items = exports.SEQUELIZE.define('vendor_order_items',{
        'id':{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
        'vendor_order_id':Sequelize.STRING,
        'product':Sequelize.STRING,
        'quantity':Sequelize.INTEGER,
        'price' : Sequelize.INTEGER
    },
        {
            timestamps: false,
            classMethods: {
                getbanners: function () {

                },
                freezeTableName: true


            }

        });
    return exports;
}
exports.initialize = initialize;
//# sourceMappingURL=sequelize-models.js.map