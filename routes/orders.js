var express = require('express');
var ordersDao = require('../dao/ordersDao');
var customerDao = require('../dao/customerDao');

var customerRepository = new customerDao.customerRepository();
var orderRepository = new ordersDao.orderRepository();

var router = express.Router();
router.use(function (req, res, next) {
    next();
});
function GetFormattedDate(date) {
    var todayTime = new Date();
    var month = (todayTime .getMonth() + 1);
    var day = (todayTime .getDate());
    var year = (todayTime .getFullYear());
    return day + "/" + month + "/" + year;
}

router.get('/', function (req, res) {
    orderRepository.findAll().then(function(data){
        res.send(data);
    })
});
router.get('/orderitems', function (req, res) {

    orderRepository.findOrderItems(req.query.id).then(function(data){
        res.send(data);
    })
});
router.post('/myorders', function (req, res) {
    customerRepository.findByPhone(req.body.phone).then(function(data){
        if(data!=null){
            orderRepository.findByEmailOrPhone(req.body.phone).then(function(data){
                var orders=[]
                for(var i=0;i<data.length;i++){
                    var total = 0;
                    var items = 0;
                    var oi = data[i].orderitems;
                    for(var j=0;j<oi.length;j++){
                        total = parseFloat(total)+ parseFloat(oi[j].price) * parseFloat(oi[j].quantity);
                        items= parseFloat(items)+ parseInt(oi[j].quantity);
                    
                    }
                    var arr = (data[i].created_at).toString().split(" ");
                    var odate = arr[0] + ","+ arr[2] +" "+arr[1] +" " + arr[3];
                    var temp =  {id:data[i].id,customer:data[i].customer,phone:data[i].phone,status:data[i].status,created_at:odate,total:total,items:items};
                        orders.push(temp);
                }
                res.send(orders);
            })
        }else{
            res.send([]); 
        }
    });
});
router.get('/allorders', function (req, res) {
    orderRepository.findAll().then(function(data){
        var orders=[]
        for(var i=0;i<data.length;i++){
            var total = 0;
            var items = 0;
            var oi = data[i].orderitems;
             for(var j=0;j<oi.length;j++){
                total = parseFloat(total)+ parseFloat(oi[j].price) * parseFloat(oi[j].quantity);
                items= parseFloat(items)+ parseInt(oi[j].quantity);
               
             }
             var arr = (data[i].created_at).toString().split(" ");
             var odate = arr[0] + ","+ arr[2] +" "+arr[1] +" " + arr[3];

              var temp =  {id:data[i].id,customer:data[i].customer,phone:data[i].phone,status:data[i].status,created_at:odate,orderitems:oi,total:total,items:items};
                orders.push(temp);
        }
        res.send(orders);
    })
});

router.post('/create', function (req, res) {
    var items = [];
    req.body.order.orderitems.forEach(function(item){
        items.push(item.product_id);
    })
    //no check customer first
    customerRepository.findByPhone(req.body.order.phone).then(function(data){
        if(data!=null){
            orderRepository.getProductStock(items).then(function(stock){
                var i=0;
                var flag =true;
                var orderItems = [];
                var message="Cannot Place Order for ";
                stock.forEach(function(prod){
                    if(prod.stock_quantity==0|| prod.stock_quantity<req.body.order.orderitems[i].quantity){
                        message+=req.body.order.orderitems[i].product+", ";
                        flag = false;
                    }
                    else{
                        orderItems.push(req.body.order.orderitems[i])
                    }
                    i++;
                })
                // req.body.order.orderitems = orderItems;
                
             
                    orderRepository.placeOrder(req.body.order).then(function(data){
                        if(flag){
                            res.send({message:"order placed successfully",status:true,partial:false});
                        }else{
                            res.send({message:message,status:true,partial:true});
                        }
                    })
                  
            })
        }
        else{
            res.send({message:"Not authorized Contact Admin",status:false,partial:true});
        }
            
    })
    
    
});
router.post('/updatestatus' , function (req, res) {
    var id = req.body.id;
    var order = {status:req.body.status};
    orderRepository.update(order,id).then(function(data){
        if(data)
        res.send({status:true});
        else
        res.send({status:false});
    })
});
router.post('/delete', function (req, res) {
    orderRepository.deleteOrder(req.body.id).then(function(data){
        if(data){
            res.send({status:true});
        }else{
            res.send({status:false});
        }
    })
});

module.exports = router;
//# sourceMappingURL=index.js.map