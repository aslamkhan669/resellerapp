var express = require('express');
var vendorsDao = require('../dao/vendorsDao.js');

var vendorsRepository = new vendorsDao.vendorsRepository();

var router = express.Router();

router.use(function(req, res, next) {
    next();
});

// router.post('/login',function(req,res){
//     vendorsRepository.login(req.body.email,req.body.phone).then(function(data){
//         res.send(data)
//     })
// });
router.get('/', function (req, res) {
    vendorsRepository.findAllVendors().then(function (data) {
        res.send(data);
    //     var vendors= [];
    //     for (var i = 0; i<data.length; i++) {
    //         for (var j = 0; j<data[i].products.length; j++) {
    //             for(var k =0; k<data[i].prices.length; k++){
    //     var p = {

    //         vendors_id : data[i].id,
    //         vendors_name : data[i].name,
    //         // vendor_pone : data[i].phone,
    //         product_id : data[i].products[j].id,
    //         product_name:data[i].products[j].product_name,
    //         // product_price : data[i].products[j].product_price,
    //         customer_id: data[i].customers[j].id,
    //         customer_name : data[i].customers[j].name,
    //         price : data[i].prices[k].product_price,
    //     }
    //     vendors.push(p);
    // }}
    // }
    //     res.send(vendors);
    })
});

router.get('/findById',function(req,res){
    vendorsRepository.findById(req.query.id).then(function(data){
        // res.send(data);
        var vendorcustomer = [];
        for(var i =0; i<data.length; i++)
        {
            for(var j = 0; j<data[i].customers.length; j++)
            {
                var vendor = { 
                vendor_name : data[i].name,
                customer_name : data[i].customers[j].name,
                customer_email : data[i].customers[j].email,
                customer_phone : data[i].customers[j].phone,
                customer_address : data[i].customers[j].address,
                customer_verified : data[i].customers[j].verified,
                }
                vendorcustomer.push(vendor);

            }
        }
        res.send(vendorcustomer);
    })
});

router.post('/authenticate', function (req, res) {
    vendorsRepository.findByPhone(req.body.phone).then(function (data) {
        if (data != null)
            res.send({ message: "authorized", status: true, vendor: data });
        else
            res.send({ message: "not authorized", status: false })
    })
});

router.post('/add',function (req , res){
    vendorsRepository.save(req.body).then(function(data){
        res.send(data);
    })
});

router.post('/update',function(req,res){
    var vendor = {

        vendor_id:req.body.vendor_id,
        name:req.body.name,
        email:req.body.email,
        password:req.body.password,
        phone:req.body.phone,
        address:req.body.address
    }
    vendorsRepository.update(vendor,req.body.id).then(function(data){
        if(data)
        res.send({status:true});
        else
        res.send({status:false});
    })
});

router.post('/deleteVendors', function(req,res){
    
    vendorsRepository.deleteVendor(req.body.id).then(function(data){
        if(data)
       res.send({status:true});
        else
        res.send({status:false});
        
    })
});
module.exports = router;