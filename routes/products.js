var express = require('express');
var productDao = require('../dao/productDao');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var router = express.Router();
var Promise = require("es6-promise").Promise;

var productRepository = new productDao.productRepository();

var router = express.Router();
router.use(function (req, res, next) {
    next();
});

router.get('/', function (req, res) {
    productRepository.findAllProducts().then(function(data){
        var products = [];
        for(var i=0;i<data.length;i++){
            for(var j=0;j<data[i].products.length;j++){
                var p = {
                    id : data[i].products[j].id,
                    product_name : data[i].products[j].product_name,
                    product_image: data[i].products[j].product_image,
                    sku : data[i].products[j].sku,
                    stock_quantity: data[i].products[j].stock_quantity,
                    product_price: data[i].products[j].product_price,
                    product_category : data[i].category_name,
                    cat_id : data[i].id,
                    product_description : data[i].products[j].product_description,
                    created_at : data[i].products[j].created_at,
                }
                products.push(p);
            }
        }
        res.send(products);
    })
});
router.post("/addProduct",function(req,res){
    var product = {};
    var form = new formidable.IncomingForm();
    form.uploadDir = "./uploads/documents";
    form.keepExtensions = true;
    form.multiples = true;
    var files = [];  
    var message = "";
    var error = ""; 
    var filename = new Date().getTime().toString();
    var fileExt = "";
        form.parse(req, function(err, fields, files) {
            User = fields.id;
            fileExt = files.product_image.name.split('.').pop();
            product = {
                product_name:fields.product_name,
                sku:fields.sku,
                stock_quantity:fields.stock_quantity,
                product_price:fields.product_price,
                product_category:fields.product_category,
                product_description:fields.product_description,
                product_image:("/"+filename+"."+fileExt)
                
            }

        });
    
        form.on('file', function(name, file) {
           
                fileExt = file.name.split('.').pop();
                //product.product_image = file.path.split("\\").split[2];
                fs.rename(file.path, form.uploadDir + "/" + filename+"."+fileExt);
                message = "files uploaded "
           
         
        });
        form.on('error', function(err) {

        });
        form.on('aborted', function() {
        });
        form.on('end', function() {
            var tableInserts = [];
            var checkPromise = []; 
               
            productRepository.save(product).then(function(data){
                if(data.id){
                    res.send({status:true});
                }else{
                    res.send({status:false});
                }
            })
            
        });


})
router.post("/updateProduct",function(req,res){
    var product = {};
    var id = "";
    var form = new formidable.IncomingForm();
    form.uploadDir = "./uploads/documents";
    form.keepExtensions = true;
    form.multiples = true;
    var files = [];  
    var message = "";
    var error = ""; 
    var filename = new Date().getTime().toString();
    var fileExt = "";
        form.parse(req, function(err, fields, files) {
           id = fields.id;
            product = {
                
                product_name:fields.product_name,
                sku:fields.sku,
                stock_quantity:fields.stock_quantity,
                product_price:fields.product_price,
                product_category:fields.product_category,
                product_description:fields.product_description,
            }
            if(files.product_image.size!=0){
                fileExt = files.product_image.name.split('.').pop();
                product.product_image = ("/"+filename+"."+fileExt);
            }
           

        });
    
        form.on('file', function(name, file) {
                if(file.size!=0){
                    fileExt = file.name.split('.').pop();
                    fs.rename(file.path, form.uploadDir + "/" + filename+"."+fileExt);
                    message = "files uploaded "
                }
                
        });
        form.on('end', function() {
            var tableInserts = [];
            var checkPromise = []; 
               
            productRepository.update(product,id).then(function(data){
                if(data){
                    res.send({status:true});
                }else{
                    res.send({status:false});
                }
            })
            
        });


})
router.get('/category', function (req, res) {
    var page = req.query.page;
    productRepository.findByCategory(req.query.id,page).then(function(data){
        res.send(data);
    })
    
});
router.get('/getcategorybyid', function (req, res) {
   
    productRepository.findCatbyId(req.query.id).then(function(data){
        res.send(data);
    })
    
});

router.get('/search', function (req, res) {
    var page = req.query.keyword;
    productRepository.findBySku(req.query.keyword).then(function(data){
        res.send(data);
    })
});
router.get('/categories', function (req, res) {
    productRepository.findAllCategories().then(function(data){
        res.send(data);
    })
});
router.get('/categories/stock', function (req, res) {
    productRepository.findAllCategoriesWithStock().then(function(data){
        res.send(data);
    })
});
router.post('/imagesbycategories', function (req, res) {
    productRepository.findAllImages(req.body.catid).then(function(data){
        res.send(data);
    })
});
router.post("/addcategory",function(req,res){
    var product = {};
    var form = new formidable.IncomingForm();
    form.uploadDir = "./uploads/documents";
    form.keepExtensions = true;
    form.multiples = true;
    var files = [];  
    var message = "";
    var error = ""; 
    var filename = new Date().getTime().toString();
    var fileExt = "";
        form.parse(req, function(err, fields, files) {
            
            fileExt = files.category_image.name.split('.').pop();
            product = {
                category_name:fields.category_name,
                parent_id:0,
                description:fields.description,
                category_image:("/"+filename+"."+fileExt),
                weight:fields.weight
                
            }

        });
    
        form.on('file', function(name, file) {
            
            
                fileExt = file.name.split('.').pop();
                //product.product_image = file.path.split("\\").split[2];
                fs.rename(file.path, form.uploadDir + "/" + filename+"."+fileExt);
                message = "files uploaded "
           
        });
        form.on('error', function(err) {

        });
        form.on('aborted', function() {
        });
        form.on('end', function() {
            var tableInserts = [];
            var checkPromise = []; 
               
            productRepository.saveCategory(product).then(function(data){
                if(data.id){
                    res.send({status:true});
                }else{
                    res.send({status:false});
                }
            })
            
        });


})
router.post("/updateCategory",function(req,res){
    var product = {};
    var form = new formidable.IncomingForm();
    form.uploadDir = "./uploads/documents";
    form.keepExtensions = true;
    form.multiples = true;
    var files = [];  
    var message = "";
    var error = ""; 
    var id = "";
    var filename = new Date().getTime().toString();
    var fileExt = "";
        form.parse(req, function(err, fields, files) {
            id = fields.id;
            product = {
                category_name:fields.category_name,
                parent_id:0,
                description:fields.description,
                weight:fields.weight
                
            }
            if(files.category_image.size!=0){
                fileExt = files.category_image.name.split('.').pop();
                product.category_image = ("/"+filename+"."+fileExt);
            }

        });
    
        form.on('file', function(name, file) {
            
                if(file.size!=0){
                fileExt = file.name.split('.').pop();
                //product.product_image = file.path.split("\\").split[2];
                fs.rename(file.path, form.uploadDir + "/" + filename+"."+fileExt);
                message = "files uploaded "
                }
           
        });
        form.on('error', function(err) {

        });
        form.on('aborted', function() {
        });
        form.on('end', function() {
            var tableInserts = [];
            var checkPromise = []; 
               
            productRepository.updateCategory(product,id).then(function(data){
                if(data){
                    res.send({status:true});
                }else{
                    res.send({status:false});
                }
            })
            
        });


})
router.post('/delete', function (req, res) {
    productRepository.deleteProduct(req.body.id).then(function(data){
        if(data){
            res.send({status:true});
        }else{
            res.send({status:false});
        }
    })
});
router.post('/category/delete', function (req, res) {
    productRepository.deleteCategory(req.body.id).then(function(data){
        if(data){
            res.send({status:true});
        }else{
            res.send({status:false});
        }
    })
});

module.exports = router;
//# sourceMappingURL=index.js.map