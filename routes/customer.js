var express = require('express');
var customerDao = require('../dao/customerDao');

var customerRepository = new customerDao.customerRepository();

var router = express.Router();
router.use(function (req, res, next) {
    next();
});

router.get('/', function (req, res) {
    customerRepository.findAll().then(function(data){
        res.send(data);
    })
});
router.post('/authenticate', function (req, res) {
    customerRepository.findByPhone(req.body.phone).then(function(data){
        if(data!=null)
            res.send({message:"authorized",status:true,customer:data});
        else
            res.send({message:"not authorized",status:false})    
    })
});
router.post('/add', function (req, res) {
    customerRepository.save(req.body).then(function(data){
        if(data)
        res.send({status:true});
        else
        res.send({status:false});
    })
});
router.post('/updatecustomer', function (req, res) {
    var customer = {
        name:req.body.name,
        email:req.body.email,
        phone:req.body.phone,
        address:req.body.address,
        verified:req.body.verified,
    }
    customerRepository.update(customer,req.body.id).then(function(data){
        if(data)
        res.send({status:true});
        else
        res.send({status:false});
    })
});
module.exports = router;
//# sourceMappingURL=index.js.map