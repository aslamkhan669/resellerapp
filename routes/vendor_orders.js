var express = require ('express')
var vendor_orderDao = require('../dao/vendor_orderDao.js');
var vendorsDao = require('../dao/vendorsDao.js');
var vendor_orderRepository = new vendor_orderDao.vendor_orderRepository();
var vendorsRepository = new vendorsDao.vendorsRepository();

var router = express.Router();
router.use(function(req, res, next){
    next();
});

router.get('/',function(req,res){
    vendor_orderRepository.findAllOrders(req.body).then(function(data){
        // res.send(data);
        var vendor_orders = [];
        for(var i=0; i<data.length; i++){
            var total_ammount = 0;
            var items = 0;
            var oi = data[i].vendor_order_items;
            for (j = 0; j<oi.length; j++){
                total_ammount = parseFloat(total_ammount)+parseFloat(oi[j].price)*parseFloat(oi[j].quantity);
                items = parseFloat(items)+parseFloat(oi[j].quantity)
            }
            var p = {
                id : data[i].vendor_id,
                vendor_name : data[i].vendor_name,
                vendor_phone : data[i].phone,
                status : data[i].status,
                vendor_order_items : oi,
                ammount:total_ammount,
                items:items,

            }
            vendor_orders.push(p);
        }
        res.send(vendor_orders)

    })
});

// router.post('/createOrder',function(req,res){
//     // var items = [];
//     // req.body.vendor_orders.vendor_order_items.forEach(function(item){
//     //     items.push(item.product_id)
//     // })
//     vendor_orderRepository.placeOrder(req.body.vendor_orders).then(function(data){
//         res.send(data)

//     })
// });

router.post('/create', function (req, res) {
    var items = [];
    req.body.vendor_orders.vendor_order_items.forEach(function(item) {
        items.push(item.product_id);
    })
    //no check customer first
    vendorsRepository.findByPhone(req.body.vendor_orders.phone).then(function (data) {
        if (data != null) {
            vendor_orderRepository.getProductStock(items).then(function (stock) {
                var i = 0;
                var flag = true;
                var orderItems = [];
                var message = "Cannot Place Order for";
                stock.forEach(function (prod) {
                    if (prod.stock_quantity == 0 || prod.stock_quantity < req.body.vendor_orders.vendor_order_items[i].quantity) {
                        message += req.body.vendor_orders.vendor_order_items[i].product + ", ";
                        flag = false;
                    }
                    else {
                        orderItems.push(req.body.vendor_orders.vendor_order_items[i])
                    }
                    i++;
                })
                req.body.vendor_orders.vendor_order_items = orderItems;


                vendor_orderRepository.placeOrder(req.body.vendor_orders).then(function (data) {
                    if (flag) {
                        res.send({ message: "order placed successfully", status: true, partial: false });
                    } else {
                        res.send({ message: message, status: true, partial: true });
                    }
                })

            })
        }
        else {
            res.send({ message: "Not authorized Contact Admin", status: false, partial: true });
        }

    })


});
module.exports = router;