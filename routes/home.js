var express = require('express');
var HomeDao = require('../dao/homeDao');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var homeRepository = new HomeDao.homeRepository();

var router = express.Router();
router.use(function (req, res, next) {
    next();
});
/* GET home page. */

router.get('/', function (req, res) {
    homeRepository.findAll().then(data=>{
        res.json(data);
    })
});
router.post("/addbanner",function(req,res){
    var banner = {};
    var form = new formidable.IncomingForm();
    form.uploadDir = "./uploads/documents";
    form.keepExtensions = true;
    form.multiples = true;
    var files = [];  
    var message = "";
    var error = ""; 
    var filename = new Date().getTime().toString();
    var fileExt = "";
        form.parse(req, function(err, fields, files) {
            
            fileExt = files.banner_image.name.split('.').pop();
            banner = {
                image:("/"+filename+"."+fileExt),
                active:1
                
            }

        });
    
        form.on('file', function(name, file) {
            
            
                fileExt = file.name.split('.').pop();
                //product.product_image = file.path.split("\\").split[2];
                fs.rename(file.path, form.uploadDir + "/" + filename+"."+fileExt);
                message = "files uploaded "
           
        });
        form.on('error', function(err) {

        });
        form.on('aborted', function() {
        });
        form.on('end', function() {
            var tableInserts = [];
            var checkPromise = []; 
               
            homeRepository.saveBanner(banner).then(function(data){
                if(data.id){
                    res.send({status:true});
                }else{
                    res.send({status:false});
                }
            })
            
        });


})
router.post("/updatebanner",function(req,res){
    var product = {};
    var form = new formidable.IncomingForm();
    form.uploadDir = "./uploads/documents";
    form.keepExtensions = true;
    form.multiples = true;
    var files = [];  
    var message = "";
    var error = ""; 
    var id = "";
    var filename = new Date().getTime().toString();
    var fileExt = "";
        form.parse(req, function(err, fields, files) {
            id = fields.id;
            banner = {
                
                active:fields.active
                
            }
            if(files.banner_image.size!=0){
                fileExt = files.banner_image.name.split('.').pop();
                banner.image = ("/"+filename+"."+fileExt);
            }

        });
    
        form.on('file', function(name, file) {
            
                if(file.size!=0){
                fileExt = file.name.split('.').pop();
                //product.product_image = file.path.split("\\").split[2];
                fs.rename(file.path, form.uploadDir + "/" + filename+"."+fileExt);
                message = "files uploaded "
                }
           
        });
        form.on('error', function(err) {

        });
        form.on('aborted', function() {
        });
        form.on('end', function() {
            var tableInserts = [];
            var checkPromise = []; 
               
            homeRepository.updateBanner(banner,id).then(function(data){
                if(data){
                    res.send({status:true});
                }else{
                    res.send({status:false});
                }
            })
            
        });


})
router.post('/delete', function (req, res) {
    homeRepository.deleteBanner(req.body.id).then(function(data){
        if(data){
            res.send({status:true});
        }else{
            res.send({status:false});
        }
    })
});

module.exports = router;
//# sourceMappingURL=index.js.map