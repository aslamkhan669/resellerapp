var models = require('../models/sequelize-models');
var dbConfig = require('../config/dbConfig');
var sequelize = require('sequelize');

var customerRepository = (function () {
    function customerRepository() {

    }
    
    customerRepository.prototype.findAll = function () {
        return models.customers.findAll();
    };
    customerRepository.prototype.findById = function (id) {
        var findOptions = {};
        findOptions.where = { id: id };
        return models.customers.find(findOptions);
    };
    customerRepository.prototype.findByPhone = function (phone) {
        var findOptions = {};
        findOptions.where = { phone: phone,verified:true };
        return models.customers.find(findOptions);
    };
    customerRepository.prototype.update = function(customer,id){
        var buildOptions = {};
        buildOptions.where = {id:id};
        return models.customers.update(customer,buildOptions);
    }
    customerRepository.prototype.save = function(customer){
        var buildOptions = {};
        return models.customers.create(customer,buildOptions);
    }
    
      
   
    return customerRepository;
})();
exports.customerRepository = customerRepository;
//# sourceMappingURL=userDao.js.map,