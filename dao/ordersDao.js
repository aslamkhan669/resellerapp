var models = require('../models/sequelize-models');
var dbConfig = require('../config/dbConfig');
var sequelize = require('sequelize');

var orderRepository = (function () {
    function orderRepository() {

    }
    
    orderRepository.prototype.findAll = function () {
        models.orders.hasMany(models.orderitems,{foreignKey:'order_id'});
        var findOptions = {};
        findOptions.include = [{ model: models.orderitems}];
        findOptions.order='created_at DESC';
        return models.orders.findAll(findOptions);
    };
    orderRepository.prototype.findById = function (id) {
        models.orders.hasMany(models.orderitems,{foreignKey:'order_id'});
        var findOptions = {};
        findOptions.include = [{ model: models.orderitems}];
        findOptions.where = {id:id};
        return models.orders.findAll(findOptions);
    };
    orderRepository.prototype.findOrderItems = function (id) {
        
        var findOptions = {};
        
        findOptions.where = {order_id:id};
        return models.orderitems.findAll(findOptions);
    };
    orderRepository.prototype.findByEmailOrPhone = function (phone) {
        models.orders.hasMany(models.orderitems,{foreignKey:'order_id'});
        var findOptions = {};
        findOptions.include = [{ model: models.orderitems}];
        findOptions.where = {$or:[{phone:phone},{email:phone}]};
        findOptions.order='created_at DESC';
        return models.orders.findAll(findOptions);
    };
    orderRepository.prototype.findByDate = function (order_date) {
        models.orders.hasMany(models.orderitems,{foreignKey:'order_id'});
        var findOptions = {};
        findOptions.include = [{ model: models.orderitems}];
        findOptions.where = {created_at:order_date};
        return models.orders.findAll(findOptions);
    };
    orderRepository.prototype.getProductStock = function (items) {
        
        var findOptions = {};
        //findOptions.include = [{ model: models.orderitems}];
        findOptions.where = {id:items};
        return models.products.findAll(findOptions);
    };
    orderRepository.prototype.update = function(order,id){
        var buildOptions = {};
        buildOptions.where = {id:id};
        return models.orders.update(order,buildOptions);
    }
    orderRepository.prototype.placeOrder = function(order){
        models.orders.hasMany(models.orderitems,{foreignKey:'order_id'});
        var buildOptions = {};
        buildOptions.include = [{model: models.orderitems, as: 'orderitems'}];
        order.orderitems.forEach(function(item){

            models.products.findById(item.product_id).then(function(product){
                if(product.stock_quantity!=0 && item.quantity<=product.stock_quantity)
                    product.decrement('stock_quantity',{by:item.quantity})
                else
                    return false;    
            })
        });
        return models.orders.create(order,buildOptions);
        
    }
    orderRepository.prototype.deleteOrder = function(oid){
        return models.orderitems.destroy({where:{order_id:oid}}).then(data =>{
            return models.orders.destroy({where:{id:oid}});
        });
    }   
      
   
    return orderRepository;
})();
exports.orderRepository = orderRepository;
//# sourceMappingURL=userDao.js.map,