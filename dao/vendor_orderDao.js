var models = require('../models/sequelize-models');
var dbConfig = require('../config/dbConfig');
var sequelize = require('sequelize');

var vendor_orderRepository = (function(){
    
    function vendor_orderRepository(){

    }

    vendor_orderRepository.prototype.findAllOrders = function(){
        models.vendor_orders.hasMany(models.vendor_order_items,{foreignKey:'vendor_order_id'})
        findoption = {};
        findoption.include = [{model:models.vendor_order_items}]
        return models.vendor_orders.findAll(findoption);
    }

    vendor_orderRepository.prototype.getProductStock = function(items){
        findoption = {};
        findoption.where = {id:items};
        return models.products.findAll(findoption)
    }

    vendor_orderRepository.prototype.placeOrder = function (order) {
        models.vendor_orders.hasMany(models.vendor_order_items, { foreignKey: 'vendor_order_id' });
        var buildOptions = {};
        buildOptions.include = [{ model: models.vendor_order_items}];
        order.vendor_order_items.forEach(function (item) {

            models.products.findById(item.product_id).then(function (product) {
                if (product.stock_quantity != 0 && item.quantity <= product.stock_quantity)
                    product.decrement('stock_quantity', { by: item.quantity })
                else
                    return false;
            })
        });
        return models.vendor_orders.create(order, buildOptions);

    }

    return vendor_orderRepository;
})();
exports.vendor_orderRepository = vendor_orderRepository;