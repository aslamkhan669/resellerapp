var models = require('../models/sequelize-models');
var dbConfig = require('../config/dbConfig');
var sequelize = require('sequelize');

var productRepository = (function () {
    function productRepository() {

    }
    
    productRepository.prototype.findAllCategories = function () {
        var findOptions = {};
      
        findOptions.order='`category`.`weight` DESC';
        return models.category.findAll(findOptions);
    };
    productRepository.prototype.findAllCategoriesWithStock = function () {
        models.category.hasMany(models.products,{foreignKey:'product_category'});
        var findOptions = {};
        findOptions.include = [{ model: models.products}];
        findOptions.order='`products`.`created_at` DESC';
        findOptions.attributes = ["id","category_name","category_image","description","parent_id","weight",[sequelize.fn('count', sequelize.col("product_category")), 'Itemscount'],[sequelize.fn('sum', sequelize.col("stock_quantity")), 'Stock']];
        findOptions.group = ["products.product_category"];
        return models.category.findAll(findOptions);
    };
    productRepository.prototype.findAllImages = function(catid){
        var findOptions = {};
        findOptions.where = { product_category: catid,stock_quantity:{$ne:0} };
        findOptions.attributes = ['product_image'];
        return models.products.findAll(findOptions);
    }


    productRepository.prototype.findAllProducts = function () {
        models.category.hasMany(models.products,{foreignKey:'product_category'});
        var findOptions = {};
        findOptions.include = [{ model: models.products}];
        findOptions.order='`products`.`stock_quantity` DESC';
        return models.category.findAll(findOptions);
    };
    productRepository.prototype.findByCategory = function (id,page) {
        var limit = 10;
        var offset = parseInt((page - 1) * parseInt(limit)) ;
        var findOptions = {};
        findOptions.where = { product_category: id };
        findOptions.order='`products`.`stock_quantity` DESC , `products`.`id`';
        findOptions.limit = limit;
        findOptions.offset =offset;
        return models.products.findAll(findOptions);
    };
    productRepository.prototype.findBySku = function (keyword) {
        
        var findOptions = {};
        findOptions.where = { sku:{$like:'%'+keyword+'%'} };
        findOptions.order='`products`.`id`';
       
        return models.products.findAll(findOptions);
    };
    productRepository.prototype.findCatbyId=function(id){
        var findOptions = {};
      
        findOptions.where = {id:id}
        return models.category.findAll(findOptions);
    }
    productRepository.prototype.findById = function (id) {
        var findOptions = {};
        findOptions.where = { id: id };
        return models.products.find(findOptions);
    };

    productRepository.prototype.save = function(product){
        var buildOptions = {};
        return models.products.create(product,buildOptions);
    }
    productRepository.prototype.update = function(product,id){
        var buildOptions = {};
        buildOptions.where = {id:id};
        return models.products.update(product,buildOptions);
    }
    productRepository.prototype.updateCategory = function(product,id){
        var buildOptions = {};
        buildOptions.where = {id:id};
        return models.category.update(product,buildOptions);
    }
    productRepository.prototype.saveCategory = function(cat){
        var buildOptions = {};
        return models.category.create(cat,buildOptions);
    }
    productRepository.prototype.deleteProduct = function(pid){
        return models.products.find({where:{
            id:pid
        }}).then(product=>{
            return product.destroy();
        })
    }  
    productRepository.prototype.deleteCategory= function(pid){
        return models.products.destroy({where:{
            product_category:pid
        }}).then(product=>{
           
               return models.category.destroy({where:{id:pid}});
           
        })
    }  
   
    return productRepository;
})();
exports.productRepository = productRepository;
//# sourceMappingURL=userDao.js.map,
