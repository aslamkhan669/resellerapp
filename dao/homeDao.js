var models = require('../models/sequelize-models');
var dbConfig = require('../config/dbConfig');
var sequelize = require('sequelize');

var homeRepository = (function () {
    function homeRepository() {

    }
    
   
    homeRepository.prototype.findAll = function () {
        var findOptions = {};
        findOptions.where = {active:1};
        findOptions.order = "id DESC";
        findOptions.limit = 4;
        return models.banner.findAll(findOptions);
    };
    homeRepository.prototype.updateBanner = function(banner,id){
        var buildOptions = {};
        buildOptions.where = {id:id};
        return models.banner.update(banner,buildOptions);
    }
    homeRepository.prototype.saveBanner = function(banner){
        var buildOptions = {};
        return models.banner.create(banner,buildOptions);
    }
  
    homeRepository.prototype.deleteBanner = function(bid){
        return models.banner.destroy({where:{id:bid}});
       
    }  
   
   
    return homeRepository;
})();
exports.homeRepository = homeRepository;
//# sourceMappingURL=userDao.js.map,