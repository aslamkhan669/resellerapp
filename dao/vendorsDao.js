var models = require('../models/sequelize-models');
var dbConfig = require('../config/dbConfig');
var sequelize = require('sequelize');

var vendorsRepository = (function () {
    function vendorsRepository() {

    }

    vendorsRepository.prototype.login = function(email,phone){
        var findOption = {};
        findOption.where = [{email:email},{phone:phone}];
        return models.vendors.find(findOption);

    };

    vendorsRepository.prototype.findAllVendors = function () {
        models.vendors.hasMany(models.customers, { foreignKey: 'vendor_id' })
        models.vendors.hasMany(models.products,{foreignKey:'vendor_id'})
        models.products.hasMany(models.prices,{foreignKey:'product_id'})
        models.vendors.hasMany(models.prices,{foreignKey:'vendor_id'})
        
        var findOptions={};
        findOptions.include = [{model:models.products},{model:models.customers},{model:models.prices}];
        return models.vendors.findAll(findOptions);
    };

    vendorsRepository.prototype.findById = function(id){
        models.vendors.hasMany(models.customers,{foreignKey:'vendor_id'})
        var findOptions = {};
        findOptions.where = {id:id};
        findOptions.include = [{model:models.customers}]
        return models.vendors.findAll(findOptions);
    };

    vendorsRepository.prototype.findByPhone = function (phone) {
        var findOptions = {};
        findOptions.where = { phone: phone, verified: true };
        return models.vendors.find(findOptions);
    };
    
    vendorsRepository.prototype.save = function(vendors){
        return models.vendors.create(vendors);
    };

    vendorsRepository.prototype.update = function(vendor,id){
        var buildOptions = {};
        buildOptions.where = {id:id};
        return models.vendors.update(vendor,buildOptions)
    }

    vendorsRepository.prototype.deleteVendor = function(vid){
        // return models.vendors.destroy({where:{id:vid}}).then(data=>{
        return models.vendors.destroy({where:{id:vid}});
    // });

}
    return vendorsRepository;
})();
exports.vendorsRepository = vendorsRepository;
//# sourceMappingURL=userDao.js.map,