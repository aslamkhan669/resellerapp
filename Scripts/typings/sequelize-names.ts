////////////////////////////////////////////////////////////////////
//
// GENERATED CLASS
//
// DO NOT EDIT
//
// See sequelize-auto-ts for edits
//
////////////////////////////////////////////////////////////////////

import types = require('./sequelize-types');

export interface SequelizeNames {
    TableNames: TableNames;
    calculatedFields:CalculatedFields;
    references:References;
    bookmarkFields:bookmarksFields;
    businessFields:businessFields;
    categoryFields:categoriesFields;
    enlistFields:enlistsFields;
    favouriteFields:favouritesFields;
    followFields:followsFields;
    messageFields:messagesFields;
    password_resetFields:password_resetsFields;
    photosFields:photosFields;
    reviewFields:reviewsFields;
    review_commentFields:review_commentsFields;
    roleFields:rolesFields;
    serviceFields:servicesFields;
    social_accountFields:social_accountsFields;
    social_account_sourceFields:social_account_sourcesFields;
    suggest_service_providerFields:suggest_service_providersFields;
    userFields:usersFields;
    user_profileFields:user_profilesFields;
}

export class TableNames {
    bookmarks:string = 'bookmarks';
    business:string = 'business';
    categories:string = 'categories';
    enlists:string = 'enlists';
    favourites:string = 'favourites';
    follows:string = 'follows';
    messages:string = 'messages';
    password_resets:string = 'password_resets';
    photos:string = 'photos';
    reviews:string = 'reviews';
    review_comments:string = 'review_comments';
    roles:string = 'roles';
    services:string = 'services';
    social_accounts:string = 'social_accounts';
    social_account_sources:string = 'social_account_sources';
    suggest_service_providers:string = 'suggest_service_providers';
    users:string = 'users';
    user_profiles:string = 'user_profiles';
}
export var tableNames:TableNames = new TableNames();

export class bookmarksFields {
    id:string = 'id';
    to_id:string = 'to_id';
    from_id:string = 'from_id';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
}
export var bookmarkFields:bookmarksFields = new bookmarksFields();


export class businessFields {
    id:string = 'id';
    user_id:string = 'user_id';
    name:string = 'name';
    address:string = 'address';
    email:string = 'email';
    phone:string = 'phone';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    user_user:string = 'user_user';
}
export var businessFields:businessFields = new businessFields();


export class categoriesFields {
    id:string = 'id';
    name:string = 'name';
    parent_id:string = 'parent_id';
    description:string = 'description';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    services:string = 'services';
    suggest_service_providers:string = 'suggest_service_providers';
}
export var categoryFields:categoriesFields = new categoriesFields();


export class enlistsFields {
    id:string = 'id';
    user_id:string = 'user_id';
    category_id:string = 'category_id';
    enlisted:string = 'enlisted';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
}
export var enlistFields:enlistsFields = new enlistsFields();


export class favouritesFields {
    id:string = 'id';
    to_id:string = 'to_id';
    from_id:string = 'from_id';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
}
export var favouriteFields:favouritesFields = new favouritesFields();


export class followsFields {
    id:string = 'id';
    follower_id:string = 'follower_id';
    following_id:string = 'following_id';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    follower_user:string = 'follower_user';
    following_user:string = 'following_user';
}
export var followFields:followsFields = new followsFields();


export class messagesFields {
    id:string = 'id';
    to_id:string = 'to_id';
    from_id:string = 'from_id';
    message:string = 'message';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    from_user:string = 'from_user';
    to_user:string = 'to_user';
}
export var messageFields:messagesFields = new messagesFields();


export class password_resetsFields {
    email:string = 'email';
    token:string = 'token';
    created_at:string = 'created_at';
}
export var password_resetFields:password_resetsFields = new password_resetsFields();


export class photosFields {
    id:string = 'id';
    user_id:string = 'user_id';
    url:string = 'url';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    user_user:string = 'user_user';
}
export var photosFields:photosFields = new photosFields();


export class reviewsFields {
    id:string = 'id';
    to_id:string = 'to_id';
    from_id:string = 'from_id';
    ratings:string = 'ratings';
    comment:string = 'comment';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    from_user:string = 'from_user';
    to_user:string = 'to_user';
}
export var reviewFields:reviewsFields = new reviewsFields();


export class review_commentsFields {
    id:string = 'id';
    review_id:string = 'review_id';
    from_id:string = 'from_id';
    comment:string = 'comment';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
}
export var review_commentFields:review_commentsFields = new review_commentsFields();


export class rolesFields {
    id:string = 'id';
    title:string = 'title';
    slug:string = 'slug';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    users:string = 'users';
}
export var roleFields:rolesFields = new rolesFields();


export class servicesFields {
    id:string = 'id';
    category_id:string = 'category_id';
    name:string = 'name';
    price:string = 'price';
    user_id:string = 'user_id';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    category_category:string = 'category_category';
    user_user:string = 'user_user';
}
export var serviceFields:servicesFields = new servicesFields();


export class social_accountsFields {
    id:string = 'id';
    provider_id:string = 'provider_id';
    username:string = 'username';
    avatar:string = 'avatar';
    souce_account_source_id:string = 'souce_account_source_id';
    user_id:string = 'user_id';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    user_user:string = 'user_user';
    souce_account_source_social_account_source:string = 'souce_account_source_social_account_source';
}
export var social_accountFields:social_accountsFields = new social_accountsFields();


export class social_account_sourcesFields {
    id:string = 'id';
    source:string = 'source';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    social_accounts:string = 'social_accounts';
}
export var social_account_sourceFields:social_account_sourcesFields = new social_account_sourcesFields();


export class suggest_service_providersFields {
    id:string = 'id';
    from_id:string = 'from_id';
    name:string = 'name';
    mobile:string = 'mobile';
    email:string = 'email';
    category:string = 'category';
    active:string = 'active';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    categocategory:string = 'categocategory';
}
export var suggest_service_providerFields:suggest_service_providersFields = new suggest_service_providersFields();


export class usersFields {
    id:string = 'id';
    email:string = 'email';
    password:string = 'password';
    seen:string = 'seen';
    active:string = 'active';
    role_id:string = 'role_id';
    confirmed:string = 'confirmed';
    confirmation_code:string = 'confirmation_code';
    remember_token:string = 'remember_token';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    business:string = 'business';
    follows:string = 'follows';
    follows:string = 'follows';
    messages:string = 'messages';
    messages:string = 'messages';
    photos:string = 'photos';
    reviews:string = 'reviews';
    reviews:string = 'reviews';
    services:string = 'services';
    social_accounts:string = 'social_accounts';
    user_profiles:string = 'user_profiles';
    user_profiles:string = 'user_profiles';
    role_role:string = 'role_role';
}
export var userFields:usersFields = new usersFields();


export class user_profilesFields {
    id:string = 'id';
    first_name:string = 'first_name';
    last_name:string = 'last_name';
    about_me:string = 'about_me';
    mobile:string = 'mobile';
    location:string = 'location';
    avatar:string = 'avatar';
    user_id:string = 'user_id';
    created_at:string = 'created_at';
    updated_at:string = 'updated_at';
    user_user:string = 'user_user';
    user_user:string = 'user_user';
}
export var user_profileFields:user_profilesFields = new user_profilesFields();


export class CalculatedFields {}
export var calculatedFields:CalculatedFields = new CalculatedFields();

export class References {
    user_id:types.Reference = { tableName: 'users', primaryKey: 'userId', foreignKey: 'user_id', as: 'User_users'};
    follower_id:types.Reference = { tableName: 'users', primaryKey: 'userId', foreignKey: 'follower_id', as: 'Follower_users'};
    following_id:types.Reference = { tableName: 'users', primaryKey: 'userId', foreignKey: 'following_id', as: 'Following_users'};
    from_id:types.Reference = { tableName: 'users', primaryKey: 'userId', foreignKey: 'from_id', as: 'From_users'};
    to_id:types.Reference = { tableName: 'users', primaryKey: 'userId', foreignKey: 'to_id', as: 'To_users'};
    category_id:types.Reference = { tableName: 'categories', primaryKey: 'categoryId', foreignKey: 'category_id', as: 'Category_categories'};
    souce_account_source_id:types.Reference = { tableName: 'social_account_sources', primaryKey: 'social_account_sourceId', foreignKey: 'souce_account_source_id', as: 'Souce_account_source_social_account_sources'};
    category:types.Reference = { tableName: 'categories', primaryKey: 'categoryId', foreignKey: 'category', as: 'Categocategories'};
    role_id:types.Reference = { tableName: 'roles', primaryKey: 'roleId', foreignKey: 'role_id', as: 'Role_roles'};
}

export var references:References = new References();
