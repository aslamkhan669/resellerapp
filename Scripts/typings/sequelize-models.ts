////////////////////////////////////////////////////////////////////
//
// GENERATED CLASS
//
// DO NOT EDIT
//
// See sequelize-auto-ts for edits
//
////////////////////////////////////////////////////////////////////

/// <reference path="./node/node.d.ts" />
/// <reference path="./sequelize/sequelize.d.ts" />

import types = require('./sequelize-types');

var Sequelize:sequelize.SequelizeStatic = require('sequelize');

export var initialized:boolean = false;
export var SEQUELIZE:sequelize.Sequelize;

export var bookmarks:types.bookmarksModel;
export var business:types.businessModel;
export var categories:types.categoriesModel;
export var enlists:types.enlistsModel;
export var favourites:types.favouritesModel;
export var follows:types.followsModel;
export var messages:types.messagesModel;
export var password_resets:types.password_resetsModel;
export var photos:types.photosModel;
export var reviews:types.reviewsModel;
export var review_comments:types.review_commentsModel;
export var roles:types.rolesModel;
export var services:types.servicesModel;
export var social_accounts:types.social_accountsModel;
export var social_account_sources:types.social_account_sourcesModel;
export var suggest_service_providers:types.suggest_service_providersModel;
export var users:types.usersModel;
export var user_profiles:types.user_profilesModel;


export function initialize(database:string, username:string, password:string, options:sequelize.Options):any
{
    if (initialized)
    {
        return;
    }

    SEQUELIZE = new Sequelize(database, username, password, options);

    bookmarks = <types.bookmarksModel> SEQUELIZE.define<types.bookmarkInstance, types.bookmarkPojo>('bookmark', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'to_id':Sequelize.INTEGER,
        'from_id':Sequelize.INTEGER,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getbookmark:(bookmark:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(bookmark);
                    if (isNaN(id)) {
                        if (bookmark['id'] !== undefined) { where['id'] = bookmark['id']}
                        if (bookmark['to_id'] !== undefined) { where['to_id'] = bookmark['to_id']}
                        if (bookmark['from_id'] !== undefined) { where['from_id'] = bookmark['from_id']}
                        if (bookmark['active'] !== undefined) { where['active'] = bookmark['active']}
                        if (bookmark['created_at'] !== undefined) { where['created_at'] = bookmark['created_at']}
                        if (bookmark['updated_at'] !== undefined) { where['updated_at'] = bookmark['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnbookmarks!!'] = id;
                    }
                    return bookmarks.find({where: where});
                }
            }
        });
    
    business = <types.businessModel> SEQUELIZE.define<types.businessInstance, types.businessPojo>('business', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'user_id':Sequelize.INTEGER,
        'name':Sequelize.STRING,
        'address':Sequelize.STRING,
        'email':Sequelize.STRING,
        'phone':Sequelize.STRING,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getbusiness:(business:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(business);
                    if (isNaN(id)) {
                        if (business['id'] !== undefined) { where['id'] = business['id']}
                        if (business['user_id'] !== undefined) { where['user_id'] = business['user_id']}
                        if (business['name'] !== undefined) { where['name'] = business['name']}
                        if (business['address'] !== undefined) { where['address'] = business['address']}
                        if (business['email'] !== undefined) { where['email'] = business['email']}
                        if (business['phone'] !== undefined) { where['phone'] = business['phone']}
                        if (business['active'] !== undefined) { where['active'] = business['active']}
                        if (business['created_at'] !== undefined) { where['created_at'] = business['created_at']}
                        if (business['updated_at'] !== undefined) { where['updated_at'] = business['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnbusiness!!'] = id;
                    }
                    return business.find({where: where});
                }
            }
        });
    
    categories = <types.categoriesModel> SEQUELIZE.define<types.categoryInstance, types.categoryPojo>('category', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'name':Sequelize.STRING,
        'parent_id':Sequelize.INTEGER,
        'description':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getcategory:(category:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(category);
                    if (isNaN(id)) {
                        if (category['id'] !== undefined) { where['id'] = category['id']}
                        if (category['name'] !== undefined) { where['name'] = category['name']}
                        if (category['parent_id'] !== undefined) { where['parent_id'] = category['parent_id']}
                        if (category['description'] !== undefined) { where['description'] = category['description']}
                        if (category['created_at'] !== undefined) { where['created_at'] = category['created_at']}
                        if (category['updated_at'] !== undefined) { where['updated_at'] = category['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOncategories!!'] = id;
                    }
                    return categories.find({where: where});
                }
            }
        });
    
    enlists = <types.enlistsModel> SEQUELIZE.define<types.enlistInstance, types.enlistPojo>('enlist', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'user_id':Sequelize.INTEGER,
        'category_id':Sequelize.INTEGER,
        'enlisted':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getenlist:(enlist:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(enlist);
                    if (isNaN(id)) {
                        if (enlist['id'] !== undefined) { where['id'] = enlist['id']}
                        if (enlist['user_id'] !== undefined) { where['user_id'] = enlist['user_id']}
                        if (enlist['category_id'] !== undefined) { where['category_id'] = enlist['category_id']}
                        if (enlist['enlisted'] !== undefined) { where['enlisted'] = enlist['enlisted']}
                        if (enlist['created_at'] !== undefined) { where['created_at'] = enlist['created_at']}
                        if (enlist['updated_at'] !== undefined) { where['updated_at'] = enlist['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnenlists!!'] = id;
                    }
                    return enlists.find({where: where});
                }
            }
        });
    
    favourites = <types.favouritesModel> SEQUELIZE.define<types.favouriteInstance, types.favouritePojo>('favourite', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'to_id':Sequelize.INTEGER,
        'from_id':Sequelize.INTEGER,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getfavourite:(favourite:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(favourite);
                    if (isNaN(id)) {
                        if (favourite['id'] !== undefined) { where['id'] = favourite['id']}
                        if (favourite['to_id'] !== undefined) { where['to_id'] = favourite['to_id']}
                        if (favourite['from_id'] !== undefined) { where['from_id'] = favourite['from_id']}
                        if (favourite['active'] !== undefined) { where['active'] = favourite['active']}
                        if (favourite['created_at'] !== undefined) { where['created_at'] = favourite['created_at']}
                        if (favourite['updated_at'] !== undefined) { where['updated_at'] = favourite['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnfavourites!!'] = id;
                    }
                    return favourites.find({where: where});
                }
            }
        });
    
    follows = <types.followsModel> SEQUELIZE.define<types.followInstance, types.followPojo>('follow', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'follower_id':Sequelize.INTEGER,
        'following_id':Sequelize.INTEGER,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getfollow:(follow:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(follow);
                    if (isNaN(id)) {
                        if (follow['id'] !== undefined) { where['id'] = follow['id']}
                        if (follow['follower_id'] !== undefined) { where['follower_id'] = follow['follower_id']}
                        if (follow['following_id'] !== undefined) { where['following_id'] = follow['following_id']}
                        if (follow['active'] !== undefined) { where['active'] = follow['active']}
                        if (follow['created_at'] !== undefined) { where['created_at'] = follow['created_at']}
                        if (follow['updated_at'] !== undefined) { where['updated_at'] = follow['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnfollows!!'] = id;
                    }
                    return follows.find({where: where});
                }
            }
        });
    
    messages = <types.messagesModel> SEQUELIZE.define<types.messageInstance, types.messagePojo>('message', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'to_id':Sequelize.INTEGER,
        'from_id':Sequelize.INTEGER,
        'message':Sequelize.STRING,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getmessage:(message:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(message);
                    if (isNaN(id)) {
                        if (message['id'] !== undefined) { where['id'] = message['id']}
                        if (message['to_id'] !== undefined) { where['to_id'] = message['to_id']}
                        if (message['from_id'] !== undefined) { where['from_id'] = message['from_id']}
                        if (message['message'] !== undefined) { where['message'] = message['message']}
                        if (message['active'] !== undefined) { where['active'] = message['active']}
                        if (message['created_at'] !== undefined) { where['created_at'] = message['created_at']}
                        if (message['updated_at'] !== undefined) { where['updated_at'] = message['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnmessages!!'] = id;
                    }
                    return messages.find({where: where});
                }
            }
        });
    
    password_resets = <types.password_resetsModel> SEQUELIZE.define<types.password_resetInstance, types.password_resetPojo>('password_reset', {
        'email':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'token':Sequelize.STRING,
        'created_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getpassword_reset:(password_reset:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(password_reset);
                    if (isNaN(id)) {
                        if (password_reset['email'] !== undefined) { where['email'] = password_reset['email']}
                        if (password_reset['token'] !== undefined) { where['token'] = password_reset['token']}
                        if (password_reset['created_at'] !== undefined) { where['created_at'] = password_reset['created_at']}
                    } else {
                        where['!!cannotFindIdFieldOnpassword_resets!!'] = id;
                    }
                    return password_resets.find({where: where});
                }
            }
        });
    
    photos = <types.photosModel> SEQUELIZE.define<types.photosInstance, types.photosPojo>('photos', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'user_id':Sequelize.INTEGER,
        'url':Sequelize.STRING,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getphotos:(photos:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(photos);
                    if (isNaN(id)) {
                        if (photos['id'] !== undefined) { where['id'] = photos['id']}
                        if (photos['user_id'] !== undefined) { where['user_id'] = photos['user_id']}
                        if (photos['url'] !== undefined) { where['url'] = photos['url']}
                        if (photos['active'] !== undefined) { where['active'] = photos['active']}
                        if (photos['created_at'] !== undefined) { where['created_at'] = photos['created_at']}
                        if (photos['updated_at'] !== undefined) { where['updated_at'] = photos['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnphotos!!'] = id;
                    }
                    return photos.find({where: where});
                }
            }
        });
    
    reviews = <types.reviewsModel> SEQUELIZE.define<types.reviewInstance, types.reviewPojo>('review', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'to_id':Sequelize.INTEGER,
        'from_id':Sequelize.INTEGER,
        'ratings':Sequelize.INTEGER,
        'comment':Sequelize.STRING,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getreview:(review:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(review);
                    if (isNaN(id)) {
                        if (review['id'] !== undefined) { where['id'] = review['id']}
                        if (review['to_id'] !== undefined) { where['to_id'] = review['to_id']}
                        if (review['from_id'] !== undefined) { where['from_id'] = review['from_id']}
                        if (review['ratings'] !== undefined) { where['ratings'] = review['ratings']}
                        if (review['comment'] !== undefined) { where['comment'] = review['comment']}
                        if (review['active'] !== undefined) { where['active'] = review['active']}
                        if (review['created_at'] !== undefined) { where['created_at'] = review['created_at']}
                        if (review['updated_at'] !== undefined) { where['updated_at'] = review['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnreviews!!'] = id;
                    }
                    return reviews.find({where: where});
                }
            }
        });
    
    review_comments = <types.review_commentsModel> SEQUELIZE.define<types.review_commentInstance, types.review_commentPojo>('review_comment', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'review_id':Sequelize.INTEGER,
        'from_id':Sequelize.INTEGER,
        'comment':Sequelize.STRING,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getreview_comment:(review_comment:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(review_comment);
                    if (isNaN(id)) {
                        if (review_comment['id'] !== undefined) { where['id'] = review_comment['id']}
                        if (review_comment['review_id'] !== undefined) { where['review_id'] = review_comment['review_id']}
                        if (review_comment['from_id'] !== undefined) { where['from_id'] = review_comment['from_id']}
                        if (review_comment['comment'] !== undefined) { where['comment'] = review_comment['comment']}
                        if (review_comment['active'] !== undefined) { where['active'] = review_comment['active']}
                        if (review_comment['created_at'] !== undefined) { where['created_at'] = review_comment['created_at']}
                        if (review_comment['updated_at'] !== undefined) { where['updated_at'] = review_comment['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnreview_comments!!'] = id;
                    }
                    return review_comments.find({where: where});
                }
            }
        });
    
    roles = <types.rolesModel> SEQUELIZE.define<types.roleInstance, types.rolePojo>('role', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'title':Sequelize.STRING,
        'slug':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getrole:(role:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(role);
                    if (isNaN(id)) {
                        if (role['id'] !== undefined) { where['id'] = role['id']}
                        if (role['title'] !== undefined) { where['title'] = role['title']}
                        if (role['slug'] !== undefined) { where['slug'] = role['slug']}
                        if (role['created_at'] !== undefined) { where['created_at'] = role['created_at']}
                        if (role['updated_at'] !== undefined) { where['updated_at'] = role['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnroles!!'] = id;
                    }
                    return roles.find({where: where});
                }
            }
        });
    
    services = <types.servicesModel> SEQUELIZE.define<types.serviceInstance, types.servicePojo>('service', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'category_id':Sequelize.INTEGER,
        'name':Sequelize.STRING,
        'price':Sequelize.DECIMAL,
        'user_id':Sequelize.INTEGER,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getservice:(service:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(service);
                    if (isNaN(id)) {
                        if (service['id'] !== undefined) { where['id'] = service['id']}
                        if (service['category_id'] !== undefined) { where['category_id'] = service['category_id']}
                        if (service['name'] !== undefined) { where['name'] = service['name']}
                        if (service['price'] !== undefined) { where['price'] = service['price']}
                        if (service['user_id'] !== undefined) { where['user_id'] = service['user_id']}
                        if (service['created_at'] !== undefined) { where['created_at'] = service['created_at']}
                        if (service['updated_at'] !== undefined) { where['updated_at'] = service['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnservices!!'] = id;
                    }
                    return services.find({where: where});
                }
            }
        });
    
    social_accounts = <types.social_accountsModel> SEQUELIZE.define<types.social_accountInstance, types.social_accountPojo>('social_account', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'provider_id':Sequelize.STRING,
        'username':Sequelize.STRING,
        'avatar':Sequelize.STRING,
        'souce_account_source_id':Sequelize.INTEGER,
        'user_id':Sequelize.INTEGER,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getsocial_account:(social_account:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(social_account);
                    if (isNaN(id)) {
                        if (social_account['id'] !== undefined) { where['id'] = social_account['id']}
                        if (social_account['provider_id'] !== undefined) { where['provider_id'] = social_account['provider_id']}
                        if (social_account['username'] !== undefined) { where['username'] = social_account['username']}
                        if (social_account['avatar'] !== undefined) { where['avatar'] = social_account['avatar']}
                        if (social_account['souce_account_source_id'] !== undefined) { where['souce_account_source_id'] = social_account['souce_account_source_id']}
                        if (social_account['user_id'] !== undefined) { where['user_id'] = social_account['user_id']}
                        if (social_account['created_at'] !== undefined) { where['created_at'] = social_account['created_at']}
                        if (social_account['updated_at'] !== undefined) { where['updated_at'] = social_account['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnsocial_accounts!!'] = id;
                    }
                    return social_accounts.find({where: where});
                }
            }
        });
    
    social_account_sources = <types.social_account_sourcesModel> SEQUELIZE.define<types.social_account_sourceInstance, types.social_account_sourcePojo>('social_account_source', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'source':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getsocial_account_source:(social_account_source:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(social_account_source);
                    if (isNaN(id)) {
                        if (social_account_source['id'] !== undefined) { where['id'] = social_account_source['id']}
                        if (social_account_source['source'] !== undefined) { where['source'] = social_account_source['source']}
                        if (social_account_source['created_at'] !== undefined) { where['created_at'] = social_account_source['created_at']}
                        if (social_account_source['updated_at'] !== undefined) { where['updated_at'] = social_account_source['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnsocial_account_sources!!'] = id;
                    }
                    return social_account_sources.find({where: where});
                }
            }
        });
    
    suggest_service_providers = <types.suggest_service_providersModel> SEQUELIZE.define<types.suggest_service_providerInstance, types.suggest_service_providerPojo>('suggest_service_provider', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'from_id':Sequelize.INTEGER,
        'name':Sequelize.STRING,
        'mobile':Sequelize.STRING,
        'email':Sequelize.STRING,
        'category':Sequelize.INTEGER,
        'active':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getsuggest_service_provider:(suggest_service_provider:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(suggest_service_provider);
                    if (isNaN(id)) {
                        if (suggest_service_provider['id'] !== undefined) { where['id'] = suggest_service_provider['id']}
                        if (suggest_service_provider['from_id'] !== undefined) { where['from_id'] = suggest_service_provider['from_id']}
                        if (suggest_service_provider['name'] !== undefined) { where['name'] = suggest_service_provider['name']}
                        if (suggest_service_provider['mobile'] !== undefined) { where['mobile'] = suggest_service_provider['mobile']}
                        if (suggest_service_provider['email'] !== undefined) { where['email'] = suggest_service_provider['email']}
                        if (suggest_service_provider['category'] !== undefined) { where['category'] = suggest_service_provider['category']}
                        if (suggest_service_provider['active'] !== undefined) { where['active'] = suggest_service_provider['active']}
                        if (suggest_service_provider['created_at'] !== undefined) { where['created_at'] = suggest_service_provider['created_at']}
                        if (suggest_service_provider['updated_at'] !== undefined) { where['updated_at'] = suggest_service_provider['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnsuggest_service_providers!!'] = id;
                    }
                    return suggest_service_providers.find({where: where});
                }
            }
        });
    
    users = <types.usersModel> SEQUELIZE.define<types.userInstance, types.userPojo>('user', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'email':Sequelize.STRING,
        'password':Sequelize.STRING,
        'seen':Sequelize.STRING,
        'active':Sequelize.STRING,
        'role_id':Sequelize.INTEGER,
        'confirmed':Sequelize.STRING,
        'confirmation_code':Sequelize.STRING,
        'remember_token':Sequelize.STRING,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getuser:(user:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(user);
                    if (isNaN(id)) {
                        if (user['id'] !== undefined) { where['id'] = user['id']}
                        if (user['email'] !== undefined) { where['email'] = user['email']}
                        if (user['password'] !== undefined) { where['password'] = user['password']}
                        if (user['seen'] !== undefined) { where['seen'] = user['seen']}
                        if (user['active'] !== undefined) { where['active'] = user['active']}
                        if (user['role_id'] !== undefined) { where['role_id'] = user['role_id']}
                        if (user['confirmed'] !== undefined) { where['confirmed'] = user['confirmed']}
                        if (user['confirmation_code'] !== undefined) { where['confirmation_code'] = user['confirmation_code']}
                        if (user['remember_token'] !== undefined) { where['remember_token'] = user['remember_token']}
                        if (user['created_at'] !== undefined) { where['created_at'] = user['created_at']}
                        if (user['updated_at'] !== undefined) { where['updated_at'] = user['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnusers!!'] = id;
                    }
                    return users.find({where: where});
                }
            }
        });
    
    user_profiles = <types.user_profilesModel> SEQUELIZE.define<types.user_profileInstance, types.user_profilePojo>('user_profile', {
        'id':{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        'first_name':Sequelize.STRING,
        'last_name':Sequelize.STRING,
        'about_me':Sequelize.STRING,
        'mobile':Sequelize.STRING,
        'location':Sequelize.STRING,
        'avatar':Sequelize.STRING,
        'user_id':Sequelize.INTEGER,
        'created_at':Sequelize.DATE,
        'updated_at':Sequelize.DATE
        },
        {
            timestamps: false,
            classMethods: {
                getuser_profile:(user_profile:any) => {
                    var where:{[key:string]:any} = {};
                    var id:number = parseInt(user_profile);
                    if (isNaN(id)) {
                        if (user_profile['id'] !== undefined) { where['id'] = user_profile['id']}
                        if (user_profile['first_name'] !== undefined) { where['first_name'] = user_profile['first_name']}
                        if (user_profile['last_name'] !== undefined) { where['last_name'] = user_profile['last_name']}
                        if (user_profile['about_me'] !== undefined) { where['about_me'] = user_profile['about_me']}
                        if (user_profile['mobile'] !== undefined) { where['mobile'] = user_profile['mobile']}
                        if (user_profile['location'] !== undefined) { where['location'] = user_profile['location']}
                        if (user_profile['avatar'] !== undefined) { where['avatar'] = user_profile['avatar']}
                        if (user_profile['user_id'] !== undefined) { where['user_id'] = user_profile['user_id']}
                        if (user_profile['created_at'] !== undefined) { where['created_at'] = user_profile['created_at']}
                        if (user_profile['updated_at'] !== undefined) { where['updated_at'] = user_profile['updated_at']}
                    } else {
                        where['!!cannotFindIdFieldOnuser_profiles!!'] = id;
                    }
                    return user_profiles.find({where: where});
                }
            }
        });
    
    users.hasMany(business, {foreignKey: 'user_id' });
    business.belongsTo(users, {as: 'User_users', foreignKey: 'user_id' });

    
    users.hasMany(follows, {foreignKey: 'follower_id' });
    follows.belongsTo(users, {as: 'Follower_users', foreignKey: 'follower_id' });

    
    users.hasMany(follows, {foreignKey: 'following_id' });
    follows.belongsTo(users, {as: 'Following_users', foreignKey: 'following_id' });

    
    users.hasMany(messages, {foreignKey: 'from_id' });
    messages.belongsTo(users, {as: 'From_users', foreignKey: 'from_id' });

    
    users.hasMany(messages, {foreignKey: 'to_id' });
    messages.belongsTo(users, {as: 'To_users', foreignKey: 'to_id' });

    
    users.hasMany(photos, {foreignKey: 'user_id' });
    photos.belongsTo(users, {as: 'User_users', foreignKey: 'user_id' });

    
    users.hasMany(reviews, {foreignKey: 'from_id' });
    reviews.belongsTo(users, {as: 'From_users', foreignKey: 'from_id' });

    
    users.hasMany(reviews, {foreignKey: 'to_id' });
    reviews.belongsTo(users, {as: 'To_users', foreignKey: 'to_id' });

    
    categories.hasMany(services, {foreignKey: 'category_id' });
    services.belongsTo(categories, {as: 'Category_categories', foreignKey: 'category_id' });

    
    users.hasMany(services, {foreignKey: 'user_id' });
    services.belongsTo(users, {as: 'User_users', foreignKey: 'user_id' });

    
    users.hasMany(social_accounts, {foreignKey: 'user_id' });
    social_accounts.belongsTo(users, {as: 'User_users', foreignKey: 'user_id' });

    
    social_account_sources.hasMany(social_accounts, {foreignKey: 'souce_account_source_id' });
    social_accounts.belongsTo(social_account_sources, {as: 'Souce_account_source_social_account_sources', foreignKey: 'souce_account_source_id' });

    
    categories.hasMany(suggest_service_providers, {foreignKey: 'category' });
    suggest_service_providers.belongsTo(categories, {as: 'Categocategories', foreignKey: 'category' });

    
    users.hasMany(user_profiles, {foreignKey: 'user_id' });
    user_profiles.belongsTo(users, {as: 'User_users', foreignKey: 'user_id' });

    
    users.hasMany(user_profiles, {foreignKey: 'user_id' });
    user_profiles.belongsTo(users, {as: 'User_users', foreignKey: 'user_id' });

    
    roles.hasMany(users, {foreignKey: 'role_id' });
    users.belongsTo(roles, {as: 'Role_roles', foreignKey: 'role_id' });

    
    return exports;
}

