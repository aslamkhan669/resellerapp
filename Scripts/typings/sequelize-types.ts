////////////////////////////////////////////////////////////////////
//
// GENERATED CLASS
//
// DO NOT EDIT
//
// See sequelize-auto-ts for edits
//
////////////////////////////////////////////////////////////////////

/// <reference path="./node/node.d.ts" />
/// <reference path="./sequelize/sequelize.d.ts" />

import types = require('./sequelize-types'); // important so we can use same fully qualified names in all generated files


var asserters:{[typeName:string]:(pojo:any, allowUndefined?:boolean) => void} = {};

//////////////////////////////////////////////////////////////////////////////
//
//
//               bookmarks
//
//
//////////////////////////////////////////////////////////////////////////////


export interface bookmarkPojo
{
    id?:number;
    to_id?:number;
    from_id?:number;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
}

export interface bookmarkInstance extends sequelize.Instance<bookmarkInstance, bookmarkPojo>, bookmarkPojo { }

export interface bookmarksModel extends sequelize.Model<bookmarkInstance, bookmarkPojo> {
    getbookmark(!!cannotFindIdFieldOnbookmarks!!:!!cannotFindIdFieldOnbookmarks!!):sequelize.PromiseT<bookmarkInstance>;
    getbookmark(bookmark:bookmarkPojo):sequelize.PromiseT<bookmarkInstance>;
}

export function assertValidbookmark(pojo:bookmarkPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid bookmark provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid bookmark provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('bookmark', 'id', pojo, 'number'); break;
            case 'to_id': assertValidFieldType('bookmark', 'to_id', pojo, 'number'); break;
            case 'from_id': assertValidFieldType('bookmark', 'from_id', pojo, 'number'); break;
            case 'active': assertValidFieldType('bookmark', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('bookmark', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('bookmark', 'updated_at', pojo, 'Date'); break;
            default:
                throw new Error('Invalid bookmark provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['bookmark'] = assertValidbookmark;





//////////////////////////////////////////////////////////////////////////////
//
//
//               business
//
//
//////////////////////////////////////////////////////////////////////////////


export interface businessPojo
{
    id?:number;
    user_id?:number;
    name?:string;
    address?:string;
    email?:string;
    phone?:string;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
    user_user?:userPojo;
}

export interface businessInstance extends sequelize.Instance<businessInstance, businessPojo>, businessPojo { }

export interface businessModel extends sequelize.Model<businessInstance, businessPojo> {
    getbusiness(!!cannotFindIdFieldOnbusiness!!:!!cannotFindIdFieldOnbusiness!!):sequelize.PromiseT<businessInstance>;
    getbusiness(business:businessPojo):sequelize.PromiseT<businessInstance>;
}

export function assertValidbusiness(pojo:businessPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid business provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid business provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('business', 'id', pojo, 'number'); break;
            case 'user_id': assertValidFieldType('business', 'user_id', pojo, 'number'); break;
            case 'name': assertValidFieldType('business', 'name', pojo, 'string'); break;
            case 'address': assertValidFieldType('business', 'address', pojo, 'string'); break;
            case 'email': assertValidFieldType('business', 'email', pojo, 'string'); break;
            case 'phone': assertValidFieldType('business', 'phone', pojo, 'string'); break;
            case 'active': assertValidFieldType('business', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('business', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('business', 'updated_at', pojo, 'Date'); break;
            case 'user_user': assertValidFieldType('business', 'user_user', pojo, 'userPojo'); break;
            default:
                throw new Error('Invalid business provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['business'] = assertValidbusiness;





//////////////////////////////////////////////////////////////////////////////
//
//
//               categories
//
//
//////////////////////////////////////////////////////////////////////////////


export interface categoryPojo
{
    id?:number;
    name?:string;
    parent_id?:number;
    description?:string;
    created_at?:Date;
    updated_at?:Date;
    services?:servicePojo[];
    suggest_service_providers?:suggest_service_providerPojo[];
}

export interface categoryInstance extends sequelize.Instance<categoryInstance, categoryPojo>, categoryPojo { }

export interface categoriesModel extends sequelize.Model<categoryInstance, categoryPojo> {
    getcategory(!!cannotFindIdFieldOncategories!!:!!cannotFindIdFieldOncategories!!):sequelize.PromiseT<categoryInstance>;
    getcategory(category:categoryPojo):sequelize.PromiseT<categoryInstance>;
}

export function assertValidcategory(pojo:categoryPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid category provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid category provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('category', 'id', pojo, 'number'); break;
            case 'name': assertValidFieldType('category', 'name', pojo, 'string'); break;
            case 'parent_id': assertValidFieldType('category', 'parent_id', pojo, 'number'); break;
            case 'description': assertValidFieldType('category', 'description', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('category', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('category', 'updated_at', pojo, 'Date'); break;
            case 'services': assertValidFieldType('category', 'services', pojo, 'servicePojo[]'); break;
            case 'suggest_service_providers': assertValidFieldType('category', 'suggest_service_providers', pojo, 'suggest_service_providerPojo[]'); break;
            default:
                throw new Error('Invalid category provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['category'] = assertValidcategory;





//////////////////////////////////////////////////////////////////////////////
//
//
//               enlists
//
//
//////////////////////////////////////////////////////////////////////////////


export interface enlistPojo
{
    id?:number;
    user_id?:number;
    category_id?:number;
    enlisted?:string;
    created_at?:Date;
    updated_at?:Date;
}

export interface enlistInstance extends sequelize.Instance<enlistInstance, enlistPojo>, enlistPojo { }

export interface enlistsModel extends sequelize.Model<enlistInstance, enlistPojo> {
    getenlist(!!cannotFindIdFieldOnenlists!!:!!cannotFindIdFieldOnenlists!!):sequelize.PromiseT<enlistInstance>;
    getenlist(enlist:enlistPojo):sequelize.PromiseT<enlistInstance>;
}

export function assertValidenlist(pojo:enlistPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid enlist provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid enlist provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('enlist', 'id', pojo, 'number'); break;
            case 'user_id': assertValidFieldType('enlist', 'user_id', pojo, 'number'); break;
            case 'category_id': assertValidFieldType('enlist', 'category_id', pojo, 'number'); break;
            case 'enlisted': assertValidFieldType('enlist', 'enlisted', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('enlist', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('enlist', 'updated_at', pojo, 'Date'); break;
            default:
                throw new Error('Invalid enlist provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['enlist'] = assertValidenlist;





//////////////////////////////////////////////////////////////////////////////
//
//
//               favourites
//
//
//////////////////////////////////////////////////////////////////////////////


export interface favouritePojo
{
    id?:number;
    to_id?:number;
    from_id?:number;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
}

export interface favouriteInstance extends sequelize.Instance<favouriteInstance, favouritePojo>, favouritePojo { }

export interface favouritesModel extends sequelize.Model<favouriteInstance, favouritePojo> {
    getfavourite(!!cannotFindIdFieldOnfavourites!!:!!cannotFindIdFieldOnfavourites!!):sequelize.PromiseT<favouriteInstance>;
    getfavourite(favourite:favouritePojo):sequelize.PromiseT<favouriteInstance>;
}

export function assertValidfavourite(pojo:favouritePojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid favourite provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid favourite provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('favourite', 'id', pojo, 'number'); break;
            case 'to_id': assertValidFieldType('favourite', 'to_id', pojo, 'number'); break;
            case 'from_id': assertValidFieldType('favourite', 'from_id', pojo, 'number'); break;
            case 'active': assertValidFieldType('favourite', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('favourite', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('favourite', 'updated_at', pojo, 'Date'); break;
            default:
                throw new Error('Invalid favourite provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['favourite'] = assertValidfavourite;





//////////////////////////////////////////////////////////////////////////////
//
//
//               follows
//
//
//////////////////////////////////////////////////////////////////////////////


export interface followPojo
{
    id?:number;
    follower_id?:number;
    following_id?:number;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
    follower_user?:userPojo;
    following_user?:userPojo;
}

export interface followInstance extends sequelize.Instance<followInstance, followPojo>, followPojo { }

export interface followsModel extends sequelize.Model<followInstance, followPojo> {
    getfollow(!!cannotFindIdFieldOnfollows!!:!!cannotFindIdFieldOnfollows!!):sequelize.PromiseT<followInstance>;
    getfollow(follow:followPojo):sequelize.PromiseT<followInstance>;
}

export function assertValidfollow(pojo:followPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid follow provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid follow provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('follow', 'id', pojo, 'number'); break;
            case 'follower_id': assertValidFieldType('follow', 'follower_id', pojo, 'number'); break;
            case 'following_id': assertValidFieldType('follow', 'following_id', pojo, 'number'); break;
            case 'active': assertValidFieldType('follow', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('follow', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('follow', 'updated_at', pojo, 'Date'); break;
            case 'follower_user': assertValidFieldType('follow', 'follower_user', pojo, 'userPojo'); break;
            case 'following_user': assertValidFieldType('follow', 'following_user', pojo, 'userPojo'); break;
            default:
                throw new Error('Invalid follow provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['follow'] = assertValidfollow;





//////////////////////////////////////////////////////////////////////////////
//
//
//               messages
//
//
//////////////////////////////////////////////////////////////////////////////


export interface messagePojo
{
    id?:number;
    to_id?:number;
    from_id?:number;
    message?:string;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
    from_user?:userPojo;
    to_user?:userPojo;
}

export interface messageInstance extends sequelize.Instance<messageInstance, messagePojo>, messagePojo { }

export interface messagesModel extends sequelize.Model<messageInstance, messagePojo> {
    getmessage(!!cannotFindIdFieldOnmessages!!:!!cannotFindIdFieldOnmessages!!):sequelize.PromiseT<messageInstance>;
    getmessage(message:messagePojo):sequelize.PromiseT<messageInstance>;
}

export function assertValidmessage(pojo:messagePojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid message provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid message provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('message', 'id', pojo, 'number'); break;
            case 'to_id': assertValidFieldType('message', 'to_id', pojo, 'number'); break;
            case 'from_id': assertValidFieldType('message', 'from_id', pojo, 'number'); break;
            case 'message': assertValidFieldType('message', 'message', pojo, 'string'); break;
            case 'active': assertValidFieldType('message', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('message', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('message', 'updated_at', pojo, 'Date'); break;
            case 'from_user': assertValidFieldType('message', 'from_user', pojo, 'userPojo'); break;
            case 'to_user': assertValidFieldType('message', 'to_user', pojo, 'userPojo'); break;
            default:
                throw new Error('Invalid message provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['message'] = assertValidmessage;





//////////////////////////////////////////////////////////////////////////////
//
//
//               password_resets
//
//
//////////////////////////////////////////////////////////////////////////////


export interface password_resetPojo
{
    email?:string;
    token?:string;
    created_at?:Date;
}

export interface password_resetInstance extends sequelize.Instance<password_resetInstance, password_resetPojo>, password_resetPojo { }

export interface password_resetsModel extends sequelize.Model<password_resetInstance, password_resetPojo> {
    getpassword_reset(!!cannotFindIdFieldOnpassword_resets!!:!!cannotFindIdFieldOnpassword_resets!!):sequelize.PromiseT<password_resetInstance>;
    getpassword_reset(password_reset:password_resetPojo):sequelize.PromiseT<password_resetInstance>;
}

export function assertValidpassword_reset(pojo:password_resetPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid password_reset provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid password_reset provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'email': assertValidFieldType('password_reset', 'email', pojo, 'string'); break;
            case 'token': assertValidFieldType('password_reset', 'token', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('password_reset', 'created_at', pojo, 'Date'); break;
            default:
                throw new Error('Invalid password_reset provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['password_reset'] = assertValidpassword_reset;





//////////////////////////////////////////////////////////////////////////////
//
//
//               photos
//
//
//////////////////////////////////////////////////////////////////////////////


export interface photosPojo
{
    id?:number;
    user_id?:number;
    url?:string;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
    user_user?:userPojo;
}

export interface photosInstance extends sequelize.Instance<photosInstance, photosPojo>, photosPojo { }

export interface photosModel extends sequelize.Model<photosInstance, photosPojo> {
    getphotos(!!cannotFindIdFieldOnphotos!!:!!cannotFindIdFieldOnphotos!!):sequelize.PromiseT<photosInstance>;
    getphotos(photos:photosPojo):sequelize.PromiseT<photosInstance>;
}

export function assertValidphotos(pojo:photosPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid photos provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid photos provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('photos', 'id', pojo, 'number'); break;
            case 'user_id': assertValidFieldType('photos', 'user_id', pojo, 'number'); break;
            case 'url': assertValidFieldType('photos', 'url', pojo, 'string'); break;
            case 'active': assertValidFieldType('photos', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('photos', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('photos', 'updated_at', pojo, 'Date'); break;
            case 'user_user': assertValidFieldType('photos', 'user_user', pojo, 'userPojo'); break;
            default:
                throw new Error('Invalid photos provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['photos'] = assertValidphotos;





//////////////////////////////////////////////////////////////////////////////
//
//
//               reviews
//
//
//////////////////////////////////////////////////////////////////////////////


export interface reviewPojo
{
    id?:number;
    to_id?:number;
    from_id?:number;
    ratings?:boolean;
    comment?:string;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
    from_user?:userPojo;
    to_user?:userPojo;
}

export interface reviewInstance extends sequelize.Instance<reviewInstance, reviewPojo>, reviewPojo { }

export interface reviewsModel extends sequelize.Model<reviewInstance, reviewPojo> {
    getreview(!!cannotFindIdFieldOnreviews!!:!!cannotFindIdFieldOnreviews!!):sequelize.PromiseT<reviewInstance>;
    getreview(review:reviewPojo):sequelize.PromiseT<reviewInstance>;
}

export function assertValidreview(pojo:reviewPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid review provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid review provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('review', 'id', pojo, 'number'); break;
            case 'to_id': assertValidFieldType('review', 'to_id', pojo, 'number'); break;
            case 'from_id': assertValidFieldType('review', 'from_id', pojo, 'number'); break;
            case 'ratings': assertValidFieldType('review', 'ratings', pojo, 'boolean'); break;
            case 'comment': assertValidFieldType('review', 'comment', pojo, 'string'); break;
            case 'active': assertValidFieldType('review', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('review', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('review', 'updated_at', pojo, 'Date'); break;
            case 'from_user': assertValidFieldType('review', 'from_user', pojo, 'userPojo'); break;
            case 'to_user': assertValidFieldType('review', 'to_user', pojo, 'userPojo'); break;
            default:
                throw new Error('Invalid review provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['review'] = assertValidreview;





//////////////////////////////////////////////////////////////////////////////
//
//
//               review_comments
//
//
//////////////////////////////////////////////////////////////////////////////


export interface review_commentPojo
{
    id?:number;
    review_id?:number;
    from_id?:number;
    comment?:string;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
}

export interface review_commentInstance extends sequelize.Instance<review_commentInstance, review_commentPojo>, review_commentPojo { }

export interface review_commentsModel extends sequelize.Model<review_commentInstance, review_commentPojo> {
    getreview_comment(!!cannotFindIdFieldOnreview_comments!!:!!cannotFindIdFieldOnreview_comments!!):sequelize.PromiseT<review_commentInstance>;
    getreview_comment(review_comment:review_commentPojo):sequelize.PromiseT<review_commentInstance>;
}

export function assertValidreview_comment(pojo:review_commentPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid review_comment provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid review_comment provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('review_comment', 'id', pojo, 'number'); break;
            case 'review_id': assertValidFieldType('review_comment', 'review_id', pojo, 'number'); break;
            case 'from_id': assertValidFieldType('review_comment', 'from_id', pojo, 'number'); break;
            case 'comment': assertValidFieldType('review_comment', 'comment', pojo, 'string'); break;
            case 'active': assertValidFieldType('review_comment', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('review_comment', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('review_comment', 'updated_at', pojo, 'Date'); break;
            default:
                throw new Error('Invalid review_comment provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['review_comment'] = assertValidreview_comment;





//////////////////////////////////////////////////////////////////////////////
//
//
//               roles
//
//
//////////////////////////////////////////////////////////////////////////////


export interface rolePojo
{
    id?:number;
    title?:string;
    slug?:string;
    created_at?:Date;
    updated_at?:Date;
    users?:userPojo[];
}

export interface roleInstance extends sequelize.Instance<roleInstance, rolePojo>, rolePojo { }

export interface rolesModel extends sequelize.Model<roleInstance, rolePojo> {
    getrole(!!cannotFindIdFieldOnroles!!:!!cannotFindIdFieldOnroles!!):sequelize.PromiseT<roleInstance>;
    getrole(role:rolePojo):sequelize.PromiseT<roleInstance>;
}

export function assertValidrole(pojo:rolePojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid role provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid role provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('role', 'id', pojo, 'number'); break;
            case 'title': assertValidFieldType('role', 'title', pojo, 'string'); break;
            case 'slug': assertValidFieldType('role', 'slug', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('role', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('role', 'updated_at', pojo, 'Date'); break;
            case 'users': assertValidFieldType('role', 'users', pojo, 'userPojo[]'); break;
            default:
                throw new Error('Invalid role provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['role'] = assertValidrole;





//////////////////////////////////////////////////////////////////////////////
//
//
//               services
//
//
//////////////////////////////////////////////////////////////////////////////


export interface servicePojo
{
    id?:number;
    category_id?:number;
    name?:string;
    price?:number;
    user_id?:number;
    created_at?:Date;
    updated_at?:Date;
    category_category?:categoryPojo;
    user_user?:userPojo;
}

export interface serviceInstance extends sequelize.Instance<serviceInstance, servicePojo>, servicePojo { }

export interface servicesModel extends sequelize.Model<serviceInstance, servicePojo> {
    getservice(!!cannotFindIdFieldOnservices!!:!!cannotFindIdFieldOnservices!!):sequelize.PromiseT<serviceInstance>;
    getservice(service:servicePojo):sequelize.PromiseT<serviceInstance>;
}

export function assertValidservice(pojo:servicePojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid service provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid service provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('service', 'id', pojo, 'number'); break;
            case 'category_id': assertValidFieldType('service', 'category_id', pojo, 'number'); break;
            case 'name': assertValidFieldType('service', 'name', pojo, 'string'); break;
            case 'price': assertValidFieldType('service', 'price', pojo, 'number'); break;
            case 'user_id': assertValidFieldType('service', 'user_id', pojo, 'number'); break;
            case 'created_at': assertValidFieldType('service', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('service', 'updated_at', pojo, 'Date'); break;
            case 'category_category': assertValidFieldType('service', 'category_category', pojo, 'categoryPojo'); break;
            case 'user_user': assertValidFieldType('service', 'user_user', pojo, 'userPojo'); break;
            default:
                throw new Error('Invalid service provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['service'] = assertValidservice;





//////////////////////////////////////////////////////////////////////////////
//
//
//               social_accounts
//
//
//////////////////////////////////////////////////////////////////////////////


export interface social_accountPojo
{
    id?:number;
    provider_id?:string;
    username?:string;
    avatar?:string;
    souce_account_source_id?:number;
    user_id?:number;
    created_at?:Date;
    updated_at?:Date;
    user_user?:userPojo;
    souce_account_source_social_account_source?:social_account_sourcePojo;
}

export interface social_accountInstance extends sequelize.Instance<social_accountInstance, social_accountPojo>, social_accountPojo { }

export interface social_accountsModel extends sequelize.Model<social_accountInstance, social_accountPojo> {
    getsocial_account(!!cannotFindIdFieldOnsocial_accounts!!:!!cannotFindIdFieldOnsocial_accounts!!):sequelize.PromiseT<social_accountInstance>;
    getsocial_account(social_account:social_accountPojo):sequelize.PromiseT<social_accountInstance>;
}

export function assertValidsocial_account(pojo:social_accountPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid social_account provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid social_account provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('social_account', 'id', pojo, 'number'); break;
            case 'provider_id': assertValidFieldType('social_account', 'provider_id', pojo, 'string'); break;
            case 'username': assertValidFieldType('social_account', 'username', pojo, 'string'); break;
            case 'avatar': assertValidFieldType('social_account', 'avatar', pojo, 'string'); break;
            case 'souce_account_source_id': assertValidFieldType('social_account', 'souce_account_source_id', pojo, 'number'); break;
            case 'user_id': assertValidFieldType('social_account', 'user_id', pojo, 'number'); break;
            case 'created_at': assertValidFieldType('social_account', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('social_account', 'updated_at', pojo, 'Date'); break;
            case 'user_user': assertValidFieldType('social_account', 'user_user', pojo, 'userPojo'); break;
            case 'souce_account_source_social_account_source': assertValidFieldType('social_account', 'souce_account_source_social_account_source', pojo, 'social_account_sourcePojo'); break;
            default:
                throw new Error('Invalid social_account provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['social_account'] = assertValidsocial_account;





//////////////////////////////////////////////////////////////////////////////
//
//
//               social_account_sources
//
//
//////////////////////////////////////////////////////////////////////////////


export interface social_account_sourcePojo
{
    id?:number;
    source?:string;
    created_at?:Date;
    updated_at?:Date;
    social_accounts?:social_accountPojo[];
}

export interface social_account_sourceInstance extends sequelize.Instance<social_account_sourceInstance, social_account_sourcePojo>, social_account_sourcePojo { }

export interface social_account_sourcesModel extends sequelize.Model<social_account_sourceInstance, social_account_sourcePojo> {
    getsocial_account_source(!!cannotFindIdFieldOnsocial_account_sources!!:!!cannotFindIdFieldOnsocial_account_sources!!):sequelize.PromiseT<social_account_sourceInstance>;
    getsocial_account_source(social_account_source:social_account_sourcePojo):sequelize.PromiseT<social_account_sourceInstance>;
}

export function assertValidsocial_account_source(pojo:social_account_sourcePojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid social_account_source provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid social_account_source provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('social_account_source', 'id', pojo, 'number'); break;
            case 'source': assertValidFieldType('social_account_source', 'source', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('social_account_source', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('social_account_source', 'updated_at', pojo, 'Date'); break;
            case 'social_accounts': assertValidFieldType('social_account_source', 'social_accounts', pojo, 'social_accountPojo[]'); break;
            default:
                throw new Error('Invalid social_account_source provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['social_account_source'] = assertValidsocial_account_source;





//////////////////////////////////////////////////////////////////////////////
//
//
//               suggest_service_providers
//
//
//////////////////////////////////////////////////////////////////////////////


export interface suggest_service_providerPojo
{
    id?:number;
    from_id?:number;
    name?:string;
    mobile?:string;
    email?:string;
    category?:number;
    active?:string;
    created_at?:Date;
    updated_at?:Date;
    categocategory?:categoryPojo;
}

export interface suggest_service_providerInstance extends sequelize.Instance<suggest_service_providerInstance, suggest_service_providerPojo>, suggest_service_providerPojo { }

export interface suggest_service_providersModel extends sequelize.Model<suggest_service_providerInstance, suggest_service_providerPojo> {
    getsuggest_service_provider(!!cannotFindIdFieldOnsuggest_service_providers!!:!!cannotFindIdFieldOnsuggest_service_providers!!):sequelize.PromiseT<suggest_service_providerInstance>;
    getsuggest_service_provider(suggest_service_provider:suggest_service_providerPojo):sequelize.PromiseT<suggest_service_providerInstance>;
}

export function assertValidsuggest_service_provider(pojo:suggest_service_providerPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid suggest_service_provider provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid suggest_service_provider provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('suggest_service_provider', 'id', pojo, 'number'); break;
            case 'from_id': assertValidFieldType('suggest_service_provider', 'from_id', pojo, 'number'); break;
            case 'name': assertValidFieldType('suggest_service_provider', 'name', pojo, 'string'); break;
            case 'mobile': assertValidFieldType('suggest_service_provider', 'mobile', pojo, 'string'); break;
            case 'email': assertValidFieldType('suggest_service_provider', 'email', pojo, 'string'); break;
            case 'category': assertValidFieldType('suggest_service_provider', 'category', pojo, 'number'); break;
            case 'active': assertValidFieldType('suggest_service_provider', 'active', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('suggest_service_provider', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('suggest_service_provider', 'updated_at', pojo, 'Date'); break;
            case 'categocategory': assertValidFieldType('suggest_service_provider', 'categocategory', pojo, 'categoryPojo'); break;
            default:
                throw new Error('Invalid suggest_service_provider provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['suggest_service_provider'] = assertValidsuggest_service_provider;





//////////////////////////////////////////////////////////////////////////////
//
//
//               users
//
//
//////////////////////////////////////////////////////////////////////////////


export interface userPojo
{
    id?:number;
    email?:string;
    password?:string;
    seen?:string;
    active?:string;
    role_id?:number;
    confirmed?:string;
    confirmation_code?:string;
    remember_token?:string;
    created_at?:Date;
    updated_at?:Date;
    business?:businessPojo[];
    follows?:followPojo[];
    follows?:followPojo[];
    messages?:messagePojo[];
    messages?:messagePojo[];
    photos?:photosPojo[];
    reviews?:reviewPojo[];
    reviews?:reviewPojo[];
    services?:servicePojo[];
    social_accounts?:social_accountPojo[];
    user_profiles?:user_profilePojo[];
    user_profiles?:user_profilePojo[];
    role_role?:rolePojo;
}

export interface userInstance extends sequelize.Instance<userInstance, userPojo>, userPojo { }

export interface usersModel extends sequelize.Model<userInstance, userPojo> {
    getuser(!!cannotFindIdFieldOnusers!!:!!cannotFindIdFieldOnusers!!):sequelize.PromiseT<userInstance>;
    getuser(user:userPojo):sequelize.PromiseT<userInstance>;
}

export function assertValiduser(pojo:userPojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid user provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid user provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('user', 'id', pojo, 'number'); break;
            case 'email': assertValidFieldType('user', 'email', pojo, 'string'); break;
            case 'password': assertValidFieldType('user', 'password', pojo, 'string'); break;
            case 'seen': assertValidFieldType('user', 'seen', pojo, 'string'); break;
            case 'active': assertValidFieldType('user', 'active', pojo, 'string'); break;
            case 'role_id': assertValidFieldType('user', 'role_id', pojo, 'number'); break;
            case 'confirmed': assertValidFieldType('user', 'confirmed', pojo, 'string'); break;
            case 'confirmation_code': assertValidFieldType('user', 'confirmation_code', pojo, 'string'); break;
            case 'remember_token': assertValidFieldType('user', 'remember_token', pojo, 'string'); break;
            case 'created_at': assertValidFieldType('user', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('user', 'updated_at', pojo, 'Date'); break;
            case 'business': assertValidFieldType('user', 'business', pojo, 'businessPojo[]'); break;
            case 'follows': assertValidFieldType('user', 'follows', pojo, 'followPojo[]'); break;
            case 'follows': assertValidFieldType('user', 'follows', pojo, 'followPojo[]'); break;
            case 'messages': assertValidFieldType('user', 'messages', pojo, 'messagePojo[]'); break;
            case 'messages': assertValidFieldType('user', 'messages', pojo, 'messagePojo[]'); break;
            case 'photos': assertValidFieldType('user', 'photos', pojo, 'photosPojo[]'); break;
            case 'reviews': assertValidFieldType('user', 'reviews', pojo, 'reviewPojo[]'); break;
            case 'reviews': assertValidFieldType('user', 'reviews', pojo, 'reviewPojo[]'); break;
            case 'services': assertValidFieldType('user', 'services', pojo, 'servicePojo[]'); break;
            case 'social_accounts': assertValidFieldType('user', 'social_accounts', pojo, 'social_accountPojo[]'); break;
            case 'user_profiles': assertValidFieldType('user', 'user_profiles', pojo, 'user_profilePojo[]'); break;
            case 'user_profiles': assertValidFieldType('user', 'user_profiles', pojo, 'user_profilePojo[]'); break;
            case 'role_role': assertValidFieldType('user', 'role_role', pojo, 'rolePojo'); break;
            default:
                throw new Error('Invalid user provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['user'] = assertValiduser;





//////////////////////////////////////////////////////////////////////////////
//
//
//               user_profiles
//
//
//////////////////////////////////////////////////////////////////////////////


export interface user_profilePojo
{
    id?:number;
    first_name?:string;
    last_name?:string;
    about_me?:string;
    mobile?:string;
    location?:string;
    avatar?:string;
    user_id?:number;
    created_at?:Date;
    updated_at?:Date;
    user_user?:userPojo;
    user_user?:userPojo;
}

export interface user_profileInstance extends sequelize.Instance<user_profileInstance, user_profilePojo>, user_profilePojo { }

export interface user_profilesModel extends sequelize.Model<user_profileInstance, user_profilePojo> {
    getuser_profile(!!cannotFindIdFieldOnuser_profiles!!:!!cannotFindIdFieldOnuser_profiles!!):sequelize.PromiseT<user_profileInstance>;
    getuser_profile(user_profile:user_profilePojo):sequelize.PromiseT<user_profileInstance>;
}

export function assertValiduser_profile(pojo:user_profilePojo, allowUndefined?:boolean):void {

    if (pojo === undefined || pojo === null) {
        if (allowUndefined) {
            return;
        }
        throw new Error('Invalid user_profile provided. It is \'' + (typeof pojo) + '\'.');
    }
    var fieldNames:string[] = Object.keys(pojo);
    if (fieldNames.length === 0) {
        throw new Error('Invalid user_profile provided. It is an empty object.');
    }

    var i:number = fieldNames.length;
    while(i-- > 0) {
        switch(fieldNames[i]) {
            case 'id': assertValidFieldType('user_profile', 'id', pojo, 'number'); break;
            case 'first_name': assertValidFieldType('user_profile', 'first_name', pojo, 'string'); break;
            case 'last_name': assertValidFieldType('user_profile', 'last_name', pojo, 'string'); break;
            case 'about_me': assertValidFieldType('user_profile', 'about_me', pojo, 'string'); break;
            case 'mobile': assertValidFieldType('user_profile', 'mobile', pojo, 'string'); break;
            case 'location': assertValidFieldType('user_profile', 'location', pojo, 'string'); break;
            case 'avatar': assertValidFieldType('user_profile', 'avatar', pojo, 'string'); break;
            case 'user_id': assertValidFieldType('user_profile', 'user_id', pojo, 'number'); break;
            case 'created_at': assertValidFieldType('user_profile', 'created_at', pojo, 'Date'); break;
            case 'updated_at': assertValidFieldType('user_profile', 'updated_at', pojo, 'Date'); break;
            case 'user_user': assertValidFieldType('user_profile', 'user_user', pojo, 'userPojo'); break;
            case 'user_user': assertValidFieldType('user_profile', 'user_user', pojo, 'userPojo'); break;
            default:
                throw new Error('Invalid user_profile provided. Field \'' + fieldNames[i] + '\' is not supported.')
        }
    }
}
asserters['user_profile'] = assertValiduser_profile;





var BOOLEAN_TYPE:string = typeof(true);
var NUMBER_TYPE:string = typeof(1);
var STRING_TYPE:string = typeof('');
var FUNCTION_TYPE:string = typeof(function() {});
var DATE_EXPECTED_TYPE:string = 'Date';
var BUFFER_EXPECTED_TYPE:string = 'Buffer';
var BUFFER_EXISTS:boolean = typeof Buffer !== 'undefined'; //in node exists, in js not, so only validate in node

function assertValidFieldType(pojoName:string, fieldName:string, pojo:any, expectedType:string):void {

    var value:any = pojo[fieldName];
    var actualType:string = typeof value;

    if (value === undefined || value === null) {
        return;
    }

    switch(expectedType) {
        case BOOLEAN_TYPE:
            if (actualType !== BOOLEAN_TYPE && actualType !== NUMBER_TYPE) {
                err();
            }
            pojo[fieldName] = Boolean(value);
            return;

        case NUMBER_TYPE:
            if (actualType === NUMBER_TYPE) {
                return;
            }
            if (actualType === STRING_TYPE) {
                var newValue:number = parseFloat(value);
                if (isNaN(newValue)) {
                    err();
                }
                pojo[fieldName] = newValue;
            }
            return;

        case STRING_TYPE:
            if (actualType !== STRING_TYPE) {
                pojo[fieldName] = value.toString();
            }
            return;

        case DATE_EXPECTED_TYPE:
            var getTime:Function = value.getTime;
            if (typeof getTime === FUNCTION_TYPE) {
                var timeValue:number = value.getTime();
                if (isNaN(timeValue)){
                    err();
                }
                if (!(value instanceof Date)) {
                    pojo[fieldName] = new Date(timeValue);
                }
                return;
            }

            if (actualType === STRING_TYPE) {
                var newDate:Date = new Date(value);
                if (!isNaN(newDate.getTime())) {
                    pojo[fieldName] = newDate;
                    return;
                }
            }
            err();
            return;

        case BUFFER_EXPECTED_TYPE:
            if (!BUFFER_EXISTS) {
                return;
            }

            if (!(value instanceof Buffer)) {
                err();
            }
            return;
    }

    // one pojo of array of array of pojos?
    if (expectedType.length > 3 && expectedType.substr(expectedType.length - 2, 2) === '[]') {
        var individualPojoType:string = expectedType.substr(0, expectedType.length - 6);

        var asserter:Function = asserters[individualPojoType];
        if (typeof asserter !== FUNCTION_TYPE) {
            err();
        }

        if (isNaN(value.length)) {
            err();
        }
        for(var i:number = 0; i<value.length; i++) {
            try {
                asserter(value[i], true);
            } catch(e) {
                err('Error at index \'' + i + '\': ' + e.message);
            }
        }

        // all instances valid
        return;
    }

    var asserter:Function = asserters[expectedType.substr(0, expectedType.length - 4)];
    if (typeof asserter !== FUNCTION_TYPE) {
        expectedTypeErr();
    }

    try {
        asserter(value, true);
    } catch(e) {
        err(e.message);
    }

    function err(extraMessage?:string):void {
        var message:string = 'Invalid ' + pojoName + ' provided. Field \'' + fieldName + '\' with value \'' + safeValue(value) + '\' is not a valid \'' + expectedType + '\'.';
        if (extraMessage !== undefined) {
            message += ' ' + extraMessage;
        }
        throw new Error(message);
    }

    function expectedTypeErr():void {
        throw new Error('Cannot validate \'' + pojoName + '\' field \'' + fieldName + '\' since expected type provided \'' + expectedType + '\' is not understood.');
    }
}

function safeValue(value:any):string {

    if (value === undefined || value === null) {
        return typeof value;
    }

    var s:string = value.toString();
    return s.substr(0, 100);
}

export interface Reference {
    tableName:string;
    primaryKey:string;
    foreignKey:string;
    as:string;
}