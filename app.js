//application dependencies
var express = require('express');
var path = require('path');
var http = require('http');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');


//initialize database connection 
var dbConfig = require('./config/dbConfig');
var _dbConfig = new dbConfig.Config();



//application routes
var rootPath = require('./routes/index');
var homePath = require('./routes/home');
var customer = require('./routes/customer');
var orders = require('./routes/orders');
var products = require('./routes/products');
var vendors = require('./routes/vendors');
var vendorOrder = require('./routes/vendor_orders');

var app = express();
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));

// app.set('views', path.join(__dirname, 'views'))
// app.set('view engine', 'ejs')
app.get('/form', function(req,res){
    // console.log("form open")
    res.render('form.jade');
});



//node mailer authentication for unsecured apps
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
process.env.TZ = "Asia/Kolkata";

app.set('port', process.env.PORT || 3000);

app.use('/api/uploads', express.static(__dirname + '/uploads'));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
//application middlewares
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());




app.use('/', rootPath);
app.use('/api/banners/', homePath);
app.use('/api/customer/', customer);
app.use('/api/order/', orders);
app.use('/api/product/', products);
app.use('/api/vendor/', vendors);
app.use('/api/vendorOrder',vendorOrder);



/// catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.status(404);
    var err = new Error('Not Found');
    err.name = '404';
    next(err);
});
/// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (handler) { return function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err.Error
        });
    }; });
}
// production error handler
// no stacktraces leaked to user
app.use(function (handler) { return function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
}; });
var newapp=http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

var io = require('socket.io')(newapp);
io.on('connection', function(socket){
     console.log('a user connected');
    socket.on('msg', function(msg){
       socket.broadcast.emit('msg', msg);
  console.log('message: ' + msg);

  });
});
module.exports = app;
//# sourceMappingURL=app.js.map
