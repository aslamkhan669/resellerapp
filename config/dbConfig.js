var models = require('../models/sequelize-models');
var Config = (function () {
    function Config() {
        var _options = {
            host: 'localhost',
            port: 3306,
            dialect: 'mysql',
            define: { freezeTableName: true }
        };
        models.initialize('reseller', 'root', '', _options);
    }
    return Config;
})();
exports.Config = Config;
//# sourceMappingURL=dbConfig.js.map
