var config = require("./app-config.json");
module.exports = {

    'facebookAuth' : {
        'clientID'      : config.facebookAppID, // your App ID
        'clientSecret'  : config.facebookAppSecret, // your App Secret
        'callbackURL'   : 'http://localhost:3000/auth/facebook/callback'
    }
};